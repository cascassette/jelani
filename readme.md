Jelani FM Synthesizer
=====================

This is an open source approach to a VST/AU FM synthesizer intended primarily for use in class rooms teaching Sound Design.

Jelani is written by Casper Ravenhorst for Sine Dubio Creative Studios, commissioned by the HKU University of the Arts Utrecht. It's shared under the GNU Public License.
