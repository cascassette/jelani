/*
  ==============================================================================

	Parameter.h
	Created: 20 Feb 2014 9:04:50pm
	Author:  Cas Cassette

	Handles parameter scaling (translating between internal and 'VST' range)

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef PARAMETER_H_INCLUDED
#define PARAMETER_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class Parameter
{
public:
	Parameter (String name, String extname, String unit, float min, float max) : name (name), extname (extname), unit (unit), minvalue (min), maxvalue (max) {}
	String getName ()				      { return name; }
	void   setName (String newname)       { name = newname; }
	String getExtName ()			      { return extname; }
	void   setExtName (String newextname) { extname = newextname; }
	String getUnit ()				      { return unit; }
	void   setUnit (String newunit)       { unit = newunit; }
	String getValue ()				      { return String (intvalue, 3) + " (" + unit + ")"; }

	float  getMinValue ()			      { return minvalue; }
	float  getMaxValue ()			      { return maxvalue; }
	void   setMinValue (float mv)	      { minvalue = mv; }
	void   setMaxValue (float mv)	      { maxvalue = mv; }

	float  getExtValue ()			      { return extvalue; }
	float  getIntValue ()			      { return intvalue; }
	virtual void setExtValue (float ev)   = 0;
	virtual void setIntValue (float iv)   = 0;

protected:
	String name, extname, unit;
	float  minvalue, maxvalue;
	float  extvalue, intvalue;
};

#endif	// PARAMETER_H_INCLUDED
