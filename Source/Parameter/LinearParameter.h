/*
  ==============================================================================

	LinearParameter.h
	Created: 20 Feb 2014 9:39:49pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef LINEARPARAMETER_H_INCLUDED
#define LINEARPARAMETER_H_INCLUDED

#include "Parameter.h"

class LinearParameter : public Parameter
{
public:
	//LinearParameter () : Parameter () {}
	LinearParameter (String name, String extname, String unit, float min, float max) : Parameter (name, extname, unit, min, max) {}
	void setExtValue (float ev);
	void setIntValue (float iv);
private:
};

#endif	// LINEARPARAMETER_H_INCLUDED
