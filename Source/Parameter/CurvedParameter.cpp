/*
  ==============================================================================

	CurvedParameter.cpp
	Created: 20 Feb 2014 10:23:08pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "CurvedParameter.h"

void CurvedParameter::setExtValue (float ev)
{
	extvalue = ev;
	intvalue = powf (ev, curve) * (maxvalue - minvalue) + minvalue;
}

void CurvedParameter::setIntValue (float /*iv*/)
{
	// nothing!: not supported in CurvedParameters
	// thus, a CP is to be used in places where no value can be typed in..
}
