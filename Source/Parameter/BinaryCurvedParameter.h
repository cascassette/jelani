/*
  ==============================================================================

    BinaryCurvedParameter.h
    Created: 4 Apr 2014 8:09:59pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef BINARYCURVEDPARAMETER_H_INCLUDED
#define BINARYCURVEDPARAMETER_H_INCLUDED

#include "Parameter.h"

class BinaryCurvedParameter : public Parameter
{
public:
	//BinaryCurvedParameter () : Parameter () {}
	BinaryCurvedParameter (String name, String extname, String unit, float min, float max, float curve) : Parameter (name, extname, unit, min, max), curve (curve) {}
	float getCurve ()		 { return curve; }
	void  setCurve(float c)  { curve = c; }
	void  setExtValue (float ev);
	void  setIntValue (float iv);
private:
	float curve;
};

#endif  // BINARYCURVEDPARAMETER_H_INCLUDED
