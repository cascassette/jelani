/*
  ==============================================================================

	DecibelParameter.cpp
	Created: 20 Feb 2014 10:17:01pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "DecibelParameter.h"
#include "../JuceLibraryCode/JuceHeader.h"

float DecibelParameter::getLinearAmp ()
{
	return Decibels::decibelsToGain (intvalue, minvalue);
}
