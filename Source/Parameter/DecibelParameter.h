/*
  ==============================================================================

	DecibelParameter.h
	Created: 20 Feb 2014 10:17:01pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef DECIBELPARAMETER_H_INCLUDED
#define DECIBELPARAMETER_H_INCLUDED

#include "LinearParameter.h"

class DecibelParameter : public LinearParameter
{
public:
	//DecibelParameter () : LinearParameter () {}
	DecibelParameter (String name, String extname, float min, float max) : LinearParameter (name, extname, "dB", min, max) {}
	float getLinearAmp ();
private:
};

#endif	// DECIBELPARAMETER_H_INCLUDED
