/*
  ==============================================================================

	SteppedParameter.h
	Created: 20 Feb 2014 10:31:44pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef STEPPEDPARAMETER_H_INCLUDED
#define STEPPEDPARAMETER_H_INCLUDED

#include "Parameter.h"

class SteppedParameter : public Parameter
{
public:
	//SteppedParameter () : Parameter () {}
	SteppedParameter (String name, String extname, String unit, float min, float max) : Parameter (name, extname, unit, min, max) {}
	void setExtValue (float ev);
	void setIntValue (float iv);
};

#endif	// STEPPEDPARAMETER_H_INCLUDED
