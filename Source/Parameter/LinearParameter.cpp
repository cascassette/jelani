/*
  ==============================================================================

	LinearParameter.cpp
	Created: 20 Feb 2014 9:39:49pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "LinearParameter.h"

void LinearParameter::setExtValue (float ev)
{
	extvalue = ev;
	intvalue = (ev * (maxvalue - minvalue)) + minvalue;
}

void LinearParameter::setIntValue (float iv)
{
	intvalue = iv;
	extvalue = (intvalue - minvalue) / (maxvalue - minvalue);
}
