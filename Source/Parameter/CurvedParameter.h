/*
  ==============================================================================

	CurvedParameter.h
	Created: 20 Feb 2014 10:23:08pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef CURVEDPARAMETER_H_INCLUDED
#define CURVEDPARAMETER_H_INCLUDED

#include "Parameter.h"

class CurvedParameter : public Parameter
{
public:
	//CurvedParameter () : Parameter () {}
	CurvedParameter (String name, String extname, String unit, float min, float max, float curve) : Parameter (name, extname, unit, min, max), curve (curve) {}
	float getCurve ()		 { return curve; }
	void  setCurve(float c)  { curve = c; }
	void  setExtValue (float ev);
	void  setIntValue (float iv);
private:
	float curve;
};

#endif	// CURVEDPARAMETER_H_INCLUDED
