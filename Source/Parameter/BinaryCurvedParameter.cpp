/*
  ==============================================================================

    BinaryCurvedParameter.cpp
    Created: 4 Apr 2014 8:09:59pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "BinaryCurvedParameter.h"

void BinaryCurvedParameter::setExtValue (float ev)
{
	extvalue = ev;
	if (extvalue == .5f)
		intvalue = 0;
	else if (extvalue < .5f)
		intvalue = powf (2.f * (.5f - ev), curve) * minvalue;
	else /* extvalue > .5 f */
		intvalue = powf (2.f * (ev - .5f), curve) * maxvalue;
}

void BinaryCurvedParameter::setIntValue (float /*iv*/)
{
	// nothing!: not supported in CurvedParameters
	// thus, a CP is to be used in places where no value can be typed in..
}