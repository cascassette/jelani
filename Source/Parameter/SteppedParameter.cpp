/*
  ==============================================================================

	SteppedParameter.cpp
	Created: 20 Feb 2014 10:31:44pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "SteppedParameter.h"

void SteppedParameter::setExtValue (float ev)
{
	extvalue = ev;
	intvalue = (float)floor((ev * (maxvalue - minvalue)) + minvalue + 0.5);
	extvalue = (intvalue - minvalue) / (maxvalue - minvalue);
}

void SteppedParameter::setIntValue (float iv)
{
	intvalue = floor(iv);
	extvalue = (intvalue - minvalue) / (maxvalue - minvalue);
}
