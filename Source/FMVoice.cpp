/*
  ==============================================================================

	FMVoice.cpp
	Created: 22 Jan 2014 3:44:30pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "FMVoice.h"
#include "PluginProcessor.h"

FMVoice::FMVoice(WaveTable* wavtab)
{
	//wt = wavtab;
	for (int i = 0; i < kOps; i++)
	{
		operators[i].setSampleRate (getSampleRate());
		operators[i].setWaveTable (wavtab);
		for (int j = 0; j < kOps; j++)
			mod[i][j] = 0.f;
	}
	pitchEnv = AD();
	pitchEnv.setSampleRate (getSampleRate());
	glideEnv = AD();
	glideEnv.setAttack (0);
	glideEnv.setSampleRate (getSampleRate());
	glideDelta = 0;
	lastPlayingNote = 0;
}

bool FMVoice::canPlaySound(SynthesiserSound* sound)
{
	return dynamic_cast <FMSound*> (sound) != 0;
}

void FMVoice::startNote (int midiNoteNumber, float velocity,
				SynthesiserSound* /*sound*/, int /*currentPitchWheelPosition*/)
{
	glideDelta = lastPlayingNote - midiNoteNumber;
	for (int i = 0; i < kOps; i++)
	{
		lastSample[i] = 0;
		operators[i].noteOn (getSampleRate(), midiNoteNumber, velocity);
	}
	pitchEnv.noteOn();
	glideEnv.noteOn();
	lastPlayingNote = midiNoteNumber;
}

int FMVoice::stopNote (bool allowTailOff)
{
	pitchEnv.noteOff();
	glideEnv.noteOff();
	if (allowTailOff)
		for (int i = 0; i < kOps; i++)
			operators[i].noteOff();
	else
		// TODO kill envelopes
		clearCurrentNote();
    return 0;
}

void FMVoice::stopNote (float velocity, bool allowTailOff)
{
    stopNote (allowTailOff);
}

void FMVoice::setParameter(int index, float newValue)
{
	if (index < JelaniFMSynthAudioProcessor::startOfOpParams)
	{
		switch (index)
		{
		// OutLevel and LFOSpeed are mo global than voices. They are uberglobal.
		case JelaniFMSynthAudioProcessor::PEAttck:	 pitchEnv.setAttack (newValue); break;
		case JelaniFMSynthAudioProcessor::PEDecay:	 pitchEnv.setDecay (newValue); break;
		case JelaniFMSynthAudioProcessor::GlideTime: glideEnv.setDecay (newValue); break;
		}
	}
	else
	{
		if (index < JelaniFMSynthAudioProcessor::startOfModParams)
		{
			int idx = index - JelaniFMSynthAudioProcessor::startOfOpParams;
			int op = idx / Operator::totalNumParamsPerOperator;
			int pr = idx % Operator::totalNumParamsPerOperator;
			switch (pr)
			{
			case Operator::Activ:	operators[op].setActiv (newValue > .5); break;
			case Operator::WForm:	operators[op].setWForm ((int)newValue); break;
			case Operator::Ratio:	operators[op].setRatio (newValue); break;
			case Operator::STOff:	operators[op].setSTOff (newValue); break;
			case Operator::HzOff:	operators[op].setHzOff (newValue); break;
			case Operator::LFOPi:	operators[op].setLFOPi (newValue); break;
			case Operator::PEAmt:	operators[op].setPEAmt (newValue); break;
			case Operator::Phase:	operators[op].setPhase (newValue); break;
			case Operator::Level:	operators[op].setLevel (newValue); break;
			case Operator::VSens:	operators[op].setVSens (newValue); break;
			case Operator::KeySc:	operators[op].setKeySc (newValue); break;
			case Operator::LFOAm:	operators[op].setLFOAm (newValue); break;
			case Operator::KTime:	operators[op].setKTime (newValue); break;
			case Operator::Attck:	operators[op].setAttck (newValue); break;
			case Operator::Decay:	operators[op].setDecay (newValue); break;
			case Operator::Sustn:	operators[op].setSustn (newValue); break;
			case Operator::Relea:	operators[op].setRelea (newValue); break;
			case Operator::PostA:	operators[op].setPostA (newValue); break;
			}
		}
		else if (index < JelaniFMSynthAudioProcessor::endOfAllParams)
		{
			int idx = index - JelaniFMSynthAudioProcessor::startOfModParams;
			int src = idx / kOps;
			int dst = idx % kOps;
			mod[src][dst] = Decibels::decibelsToGain (newValue, opModulMinInfdB);
		}
	}
}

void FMVoice::setLFOBuffer (double* newLFOBuffer, int /*numSamples*/)
{
	LFOBuffer = newLFOBuffer;
}

double FMVoice::getNextSample(double lfoVal)
{
	// Pitch Envelope value
	double peVal = pitchEnv.process();
	// Glide Env value
	double glVal = glideEnv.process() * glideDelta;
	// Since we're adding up operator results we'll start at zero
	double res = 0.0f;
	// Keeps operator outputs (pre mixer sts) to save in lastSample buffer
	double sampleBuffers[kOps];

	for (int i = 0; i < kOps; i++)
	{
		if (operators[i].getActiv())
		{
			double phaseMod = 0.0;
			for (int j = 0; j < kOps; j++)
			{
				if (operators[j].getActiv())
					phaseMod += (double)mod[j][i] * lastSample[j];
			}
			sampleBuffers[i] = operators[i].nextSample (phaseMod, lfoVal, peVal, glVal);
			res += sampleBuffers[i] * operators[i].getPostA();
		}
	}
	for (int i = 0; i < kOps; i++)
		lastSample[i] = sampleBuffers[i];

	return res;
}

void FMVoice::renderNextBlock(AudioSampleBuffer& outputBuffer, int startSample, int numSamples)
{
	if (!isEnvDone())
	{
		while (--numSamples >= 0)
		{
			const float currentSample = (float)getNextSample(LFOBuffer[startSample]);

			for (int i = outputBuffer.getNumChannels(); --i >= 0;)
				//*outputBuffer.getSampleData(i, startSample) += currentSample;
                *outputBuffer.getWritePointer(i, startSample) += currentSample;

			++startSample;
		}
	}
	else
		clearCurrentNote();
}

bool FMVoice::isEnvDone()
{
	bool res = true;
	for (int op = 0; op < kOps; op++)
		res &= (!operators[op].getActiv() || operators[op].getPostA() == 0 || operators[op].isEnvDone());
	return res;
}
