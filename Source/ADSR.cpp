/*
  ==============================================================================

	ADSR.cpp
	Created: 30 Jan 2014 9:05:51pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "ADSR.h"
#include "const.h"
#include "../JuceLibraryCode/JuceHeader.h"

ADSR::ADSR ()
{
	reset();
	factor = 1.f;
	setAttack (0.f);
	setDecay (0.f);
	setRelease (0.f);
	setSustain (1.f);
}

void ADSR::setSampleRate (double sampleRate)
{
	spms = sampleRate / 1000.f;
	setAttack (0.f);
	setDecay (0.f);
	setRelease (0.f);
	setSustain (1.f);
}

void ADSR::noteOn (float f)
{
	//reset();

	factor = f;
	state = Attack;
	// anti-click
	if (att <= 2.f) // if shorter than 2 samples we'll just skip the attack phase right away
	{
		output = 1.f;
		state = Decay;
		if (dec <= 2.f) // same
		{
			output = sus;
			state = Sustain;
		}
	}
	isNoteOn = true;
}

void ADSR::noteOff()
{
	isNoteOn = false;
	if (state != Idle)
	{
		if (rel != 0.f)
			deltaGRel = (0.f - output) / rel;
		else
			deltaGRel = -25000.f;

		state = Release;
	}
}

float ADSR::process ()
{
	switch (state)
	{
	case Idle:
		break;
	case Attack:
		output += (float)deltaGAtt * factor;
		if (output >= 1.f)
		{
			output = 1.f;
			state = Decay;
		}
		break;
	case Decay:
		output += (float)deltaGDec * factor;
		if (output <= sus)
		{
			output = sus;
			state = Sustain;
		}
		else if (output <= 0.f)
		{
			output = 0.f;
			state = Idle;
		}
		break;
	case Sustain:
		output = sus;
		if (!isNoteOn) state = Release;
		break;
	case Release:
		output += (float)deltaGRel * factor;
		if (output < 0.f)
		{
			output = 0.f;
			state = Idle;
		}
		break;
	}
	return output;
}

void ADSR::setAttack (float newAtt)
{
	att = (float)(newAtt * spms);
	if (att != 0.f)
		deltaGAtt = (1.f - 0.f) / att;
	else
		deltaGAtt = 25000.f;
}

void ADSR::setDecay (float newDec)
{
	dec = (float)(newDec * spms);
	if (dec != 0.f)
		deltaGDec = (sus - 1.f) / dec;
	else
		deltaGDec = -25000.f;
}

void ADSR::setSustain (float newSus)
{
	float levelWanted = Decibels::gainToDecibels (newSus, opSustnMinInfdB);
	levelWanted += sustainRangedB;
	levelWanted /= sustainRangedB;
	sus = levelWanted;

	if (dec != 0.f)
		deltaGDec = (sus - 1.f) / dec;
	else
		deltaGDec = -25000.f;

	if (rel != 0.f)
		deltaGRel = (0.f - sus) / rel;
	else
		deltaGRel = -25000.f;
}

void ADSR::setRelease (float newRel)
{
	rel = (float)(newRel * spms);
	if (rel != 0.f)
		deltaGRel = (0.f - sus) / rel;
	else
		deltaGRel = -25000.f;
}

void ADSR::reset ()
{
	state = Idle;
	isNoteOn = false;
	output = 0.f;
}