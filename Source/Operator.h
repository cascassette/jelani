/*
  ==============================================================================

	Operator.h
	Created: 22 Jan 2014 12:26:17am
	Author:  Cas Cassette

	Handles all per-operator settings and provides wavetable readout

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef OPERATOR_H_INCLUDED
#define OPERATOR_H_INCLUDED

#include "ADSR.h"
#include "WaveTable.h"

class Operator
{
public:
	Operator ();

	void setWaveTable (WaveTable* wavtab);
	void setSampleRate (double sampleRate);

	// Getters & setters
	bool  getActiv() { return activ; }
	int   getWForm() { return wform; }
	float getRatio() { return ratio; }
	float getSTOff() { return stoff; }
	float getHzOff() { return hzoff; }
	float getLFOPi() { return lfopi; }
	float getPEAmt() { return peamt; }
	float getPhase() { return phase; }
	float getLevel() { return level; }
	float getVSens() { return vsens; }
	float getKeySc() { return keysc; }
	float getLFOAm() { return lfoam; }
	float getKTime() { return ktime; }
	float getAttck() { return envgen.getAttack();  }
	float getDecay() { return envgen.getDecay();   }
	float getSustn() { return envgen.getSustain(); }
	float getRelea() { return envgen.getRelease(); }
	float getPostA() { return posta; }

	void setActiv (bool a);
	void setWForm (int w);
	void setRatio (float r);
	void setSTOff (float o);
	void setHzOff (float h);
	void setLFOPi (float p);
	void setPEAmt (float a);
	void setPhase (float p);
	void setLevel (float l);
	void setVSens (float v);
	void setKeySc (float k);
	void setLFOAm (float a);
	void setKTime (float k);
	void setAttck (float att);
	void setDecay (float dec);
	void setSustn (float sus);
	void setRelea (float rel);
	void setPostA (float a);

	// Note on; calculate amp and pitch and generate waves
	void noteOn (double sampleRate, int midiNoteNumber, float velocity);
	// Envelope handles note off
	void noteOff();
	// Via this chaining function here
	bool isEnvDone()  { return envgen.getState() == 0; }

	// Advances one sample and returns it
	double nextSample (double deltaPhase = 0.0, double lfoVal = 0.0, double peVal = 0.0, double glVal = 0.0);
	// Just gives current sample
	double currentSample (double deltaPhase = 0.0, double lfoVal = 0.0);

	enum ParametersPerOperator
	{
		Activ = 0,
		WForm,
		Ratio,
		STOff,
		HzOff,
		PEAmt,
		Phase,
		Level,
		VSens,
		KeySc,
		LFOAm,
		LFOPi,
		KTime,
		Attck,
		Decay,
		Sustn,
		Relea,
		PostA,

		totalNumParamsPerOperator
	};

	enum WForms
	{
		Sine = 0,
		Sine12,
		Sine13,
		Sine123Saw,
		Sine135Squ,
		Sine14,
		Sine15,
		Sine16,
		Sine17,
		Sine18,
		Triangle,
		Square,
		Saw,
		Parabol,
		SoftSquare
	};

	// Static property names for GUI (might want to shove these over to OperatorGUI)
	static const char* getOperatorName (int o);
	static const char* getParameterName (int p);
	static const char* getFriendlyParameterName (int p);
	static const char* getFriendlyParameterUnit (int p);
	static const char* getFriendlyWFName (int w);

private:
	// Helper functions used in setNoteOn
	float getVolumeScaling (int midiNoteNumber);
	float getTimeScaling (int midiNoteNumber);
	double getMidiNoteInHertz (float noteNumber, const double frequencyOfA = 440.0);
	double getMidiIntervalInRatio (double noteInterval);
	// Pointer to currently set waveform
	WaveTable* wt;
	// Pointer to first of all possible waveforms
	WaveTable* allwt;
	// Envelope
	ADSR  envgen;
	// Operator properties (all accessible from GUI and host)
	bool  activ;   // operator enabled
	int   wform;   // waveform
	float ratio;   // ratio
	float stoff;   // semitone offset
	float hzoff;   // pitch offset in hz
	float lfopi;   // lfo > pitch depth
	float peamt;   // pitch env > pitch depth
	float phase;   // start phase
	float level;   // global amp before modulation
	float vsens;   // velocity sensitivity
	float keysc;   // key > level scaling factor
	float lfoam;   // lfo > amp (before modulation) depth
	float ktime;   // key > envelope time scaling factor
	float posta;   // out amp (operator functions as modulator only if zero)
	// Double precision for internal calculations
	double currentPhase, phaseDelta, amp, tailOff;
};

#endif	// OPERATOR_H_INCLUDED
