/*
  ==============================================================================

    AD.cpp
    Created: 28 Feb 2014 2:59:59pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "AD.h"
#include "const.h"
#include <math.h>

AD::AD ()
{
	reset ();
	setAttack (0);
	setDecay (0);
}

void AD::setSampleRate (double sampleRate)
{
	spms = sampleRate / 1000.f;
	setAttack (0);
	setDecay (0);
}

void AD::noteOn ()
{
	reset();

	state = Attack;
	if (att < 2.f)
	{
		output = 1.f;
		state = Decay;
	}
}

void AD::noteOff ()
{
}

float AD::process ()
{
	switch (state)
	{
	case Idle:
		break;
	case Attack:
		output += (float)deltaGAtt;
		if (output >= 1.f)
		{
			output = 1.f;
			state = Decay;
		}
		break;
	case Decay:
		output += (float)deltaGDec;
		if (output <= 0.f)
		{
			output = 0.f;
			state = Idle;
		}
		break;
	}
	return output;
}

void AD::setAttack (float newAtt)
{
	att = (float)((double)newAtt * spms);
	if (att != 0.f)
		deltaGAtt = (1.f - 0.f) / att;
	else
		deltaGAtt = 25000.f;
}

void AD::setDecay (float newDec)
{
	dec = (float)((double)newDec * spms);
	if (dec != 0.f)
		deltaGDec = (0.f - 1.f) / dec;
	else
		deltaGDec = -25000.f;
}

void AD::reset ()
{
	state = Idle;
	output = 0.f;
}