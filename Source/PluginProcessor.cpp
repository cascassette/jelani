/*
  ==============================================================================

	PluginProcessor.cpp
	Created: 7 Jan 2014 10:14:23am
	Author:  Cas Cassette

	Plugin Processor code.

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "FMVoice.h"
#include "const.h"
#include "Parameter/DecibelParameter.h"
#include "Parameter/SteppedParameter.h"
#include "Parameter/CurvedParameter.h"
#include "Parameter/BinaryCurvedParameter.h"

AudioProcessor* JUCE_CALLTYPE createPluginFilter();

static float Waves[kWaveForms][kWaveSize];

inline float _max (float a, float b) {
	return (a > b) ? a : b;
}

JelaniFMSynthAudioProcessor::JelaniFMSynthAudioProcessor()
{
	// Init patches
	settingsDirectory = File::getSpecialLocation (File::userApplicationDataDirectory).getChildFile ("SineDubio/Jelani/patches");
	settingsDirectory.createDirectory();
	programCount = settingsDirectory.getNumberOfChildFiles (File::findFiles, "*.xml");
	DirectoryIterator programFinder (settingsDirectory, false, "*.xml");
	int i = 0;
	while (programFinder.next())
	{
		File pgmfile = programFinder.getFile();
		programnames.add (pgmfile.getFileNameWithoutExtension());
		//programs.add (pgmfile.loadFileAsString());
		i++;
	}
	programLoaded = 0;

	// Init global LFO
	bpm = 90.2;
	globalLFO = LFO();
	LFOBufInitialised = false;

	// Set up normalise
	float maxdev[kWaveForms];
	for (int i = 0; i < kWaveForms; i++)
		maxdev[i] = 0.f;

	// Init wavetables
	//double last_ssqu = -1.;
	//double slowgrowth = (double)powl (.995, kWaveSize / 4);
	//double superslowgrowth = (double)powl (.995, kWaveSize / 2.5);
	//bool ssqustate = true;
	for (int i = 0; i < kWaveSize; i++)
	{
		double sin1 = sin (2.0 * double_Pi*((double)i / (double)kWaveSize));
		double sin2 = (sin (4.0 * double_Pi*((double)i / (double)kWaveSize)) / 2.0);
		double sin3 = (sin (6.0 * double_Pi*((double)i / (double)kWaveSize)) / 3.0);
		double sin4 = (sin (8.0 * double_Pi*((double)i / (double)kWaveSize)) / 4.0);
		double sin5 = (sin (10.0 * double_Pi*((double)i / (double)kWaveSize)) / 5.0);
		double sin6 = (sin (12.0 * double_Pi*((double)i / (double)kWaveSize)) / 6.0);
		double sin7 = (sin (14.0 * double_Pi*((double)i / (double)kWaveSize)) / 7.0);
		double sin8 = (sin (16.0 * double_Pi*((double)i / (double)kWaveSize)) / 8.0);
		double tri = fabs (2.0 - fabs (1.0 - 4.0 * ((double)i / (double)kWaveSize))) - 1.0;
		double squ = (i < kWaveSize / 2) ? 1. : -1.;
		double saw = 1. - 2. * ((double)i / (double)kWaveSize);
		double par = pow (1. - fabs (tri), 2) * -squ + squ;
		double ssqu = (sin1 + sin3 + sin5 + sin7) + .25 * (sin2 + sin4 + sin6/* + sin8*/);
		//double ssqu;
		/*if (abs (last_ssqu - squ) > 1.) ssqustate = true;
		if (ssqustate) // rising to squ, or 'attacking'
		{
			bool dir = squ > last_ssqu;
			ssqu = last_ssqu + (squ - last_ssqu) * slowgrowth + squ * .0015;
			if ((dir && ssqu > squ) || (!dir && ssqu < squ))
			{
				ssqu = squ;
				ssqustate = false;
			}
		}
		else
		{
			bool dir = ssqu < 0;
			ssqu -= ssqu * superslowgrowth;
			if ((dir && ssqu > 0) || (!dir && ssqu < 0))
				ssqu = 0.;
		}
		last_ssqu = ssqu;*/
		Waves[Operator::Sine][i]	   = (float)(sin1);
		Waves[Operator::Sine12][i]	   = (float)(sin1 + sin2);
		Waves[Operator::Sine13][i]	   = (float)(sin1 + sin3);
		Waves[Operator::Sine123Saw][i] = (float)(sin1 + sin2 + sin3);
		Waves[Operator::Sine135Squ][i] = (float)(sin1 + sin3 + sin5);
		Waves[Operator::Sine14][i]	   = (float)(sin1 + sin4);
		Waves[Operator::Sine15][i]	   = (float)(sin1 + sin5);
		Waves[Operator::Sine16][i]	   = (float)(sin1 + sin6);
		Waves[Operator::Sine17][i]	   = (float)(sin1 + sin7);
		Waves[Operator::Sine18][i]	   = (float)(sin1 + sin8);
		Waves[Operator::Triangle][i]   = (float)(tri);
		Waves[Operator::Square][i]     = (float)(squ);
		Waves[Operator::Saw][i]        = (float)(saw);
		Waves[Operator::Parabol][i]    = (float)(par);
		Waves[Operator::SoftSquare][i] = (float)(ssqu);
	}

	// Normalise - determine max deviation from 0
	for (int i = 0; i < kWaveForms; i++)
	{
		for (int j = 0; j < kWaveSize; j++)
		{
			maxdev[i] = _max (maxdev[i], fabs (Waves[i][j]));
		}
	}
	// Normalise - revert back from that max deviation
	for (int i = 0; i < kWaveForms; i++)
	{
		for (int j = 0; j < kWaveSize; j++)
			Waves[i][j] /= maxdev[i];
	}

	// Load into WaveTables
	for (int i = 0; i < kWaveForms; i++)
		WaveTables[i] = WaveTable (Waves[i], kWaveSize);

	// Initialise parameter containers
	Params[OutLevel] = new DecibelParameter ("Out", "OUT", outLevelMinInfdB, 0);
	Params[LFOSpeed] = new LinearParameter ("LFO Speed", "LFOSPD", "Hz", 0, maxLFOSpeed);
	Params[PEAttck] = new LinearParameter ("Attack", "PEATTCK", "ms", 0, maxAttPitch);
	Params[PEDecay] = new LinearParameter ("Decay", "PEDECAY", "ms", 0, maxDecPitch);
	Params[NumVoices] = new SteppedParameter ("Voices", "NVOICES", "", 1, maxVoices);
	Params[LFOSync] = new SteppedParameter ("Sync On", "LFOSYNC", "", 0, 1);
	Params[LFOSyncId] = new SteppedParameter ("Sync speed", "LFOSYNCSPEED", "", LFO::Four, LFO::One32nd);
	Params[LFOSyncMultId] = new SteppedParameter ("Sync mode", "LFOSYNCMULTI", "", LFO::Dotted, LFO::Triplet);
	Params[GlideTime] = new CurvedParameter ("Glide", "GLIDETIME", "ms", 0, maxGlideTime, 2.f);
	for (int i = 0; i < kOps; i++)
	{
		String opName = Operator::getOperatorName (i);
		// set stepped parameters
		opParams[i][Operator::Activ] = new SteppedParameter ("On", opName + " " + Operator::getParameterName (Operator::Activ), "", 0, 1);
		opParams[i][Operator::WForm] = new SteppedParameter ("Waveform", opName + " " + Operator::getParameterName (Operator::WForm), "", 0, kWaveForms - 1);
		// set linear parameters
		opParams[i][Operator::Ratio] = new LinearParameter ("Ratio", opName + " " + Operator::getParameterName (Operator::Ratio), "", 0, maxRatio);
		opParams[i][Operator::STOff] = new LinearParameter ("Semitone", opName + " " + Operator::getParameterName (Operator::STOff), "st", -60, 60);
		opParams[i][Operator::HzOff] = new LinearParameter ("Hz Offset", opName + " " + Operator::getParameterName (Operator::HzOff), "Hz", -10000, 10000);
		opParams[i][Operator::Phase] = new LinearParameter ("Phase Offset", opName + " " + Operator::getParameterName (Operator::Phase), "", 0, 1);
		opParams[i][Operator::VSens] = new LinearParameter ("Velocity Sensitivity", opName + " " + Operator::getParameterName (Operator::VSens), "", 0, 1);
		opParams[i][Operator::KeySc] = new LinearParameter ("Keyscaling", opName + " " + Operator::getParameterName (Operator::KeySc), "", -3, 3);
		opParams[i][Operator::KTime] = new LinearParameter ("Key to Envelope speed", opName + " " + Operator::getParameterName (Operator::KTime), "", -1, 1);
		opParams[i][Operator::Sustn] = new LinearParameter ("Sustain", opName + " " + Operator::getParameterName (Operator::Sustn), "", 0, 1);
		// set curved parameters
		opParams[i][Operator::Attck] = new CurvedParameter ("Attack", opName + " " + Operator::getParameterName (Operator::Attck), "ms", 0, maxAtt, 2.f);
		opParams[i][Operator::Decay] = new CurvedParameter ("Decay", opName + " " + Operator::getParameterName (Operator::Decay), "ms", 0, maxDec, 2.f);
		opParams[i][Operator::Relea] = new CurvedParameter ("Release", opName + " " + Operator::getParameterName (Operator::Relea), "ms", 0, maxRel, 2.f);
		// set binary curved parameters
		opParams[i][Operator::PEAmt] = new BinaryCurvedParameter ("Pitch Env Amount", opName + " " + Operator::getParameterName (Operator::PEAmt), "st", -12, 12, 4.f);
		opParams[i][Operator::LFOPi] = new BinaryCurvedParameter ("LFO Amount", opName + " " + Operator::getParameterName (Operator::LFOPi), "st", -2, 2, 4.f);
		opParams[i][Operator::LFOAm] = new BinaryCurvedParameter ("LFO Amount", opName + " " + Operator::getParameterName (Operator::LFOAm), "dB", -12, 12, 2.f);
		// set decibel parameters
		opParams[i][Operator::Level] = new DecibelParameter ("Level", opName + " " + Operator::getParameterName (Operator::Level), opLevelMinInfdB, 0);
		opParams[i][Operator::PostA] = new DecibelParameter ("Out Level", opName + " " + Operator::getParameterName (Operator::PostA), opPostAMinInfdB, 0);

		for (int j = 0; j < kOps; j++)
		{
			modParams[i][j] = new DecibelParameter (String ("Mod ") + opName + ">" + Operator::getOperatorName (j),
				String ("MOD ") + opName + ">" + Operator::getOperatorName (j), opModulMinInfdB, maxModdB);
		}
	}
	// Set all parameters to default
	paramReset();

	// Add synth elements
	glide = false;
	//setNumVoices (defaultNumVoices);
	synth.addSound (new FMSound ());

	// Should refresh on create? Sure
	UIUpdateFlag = true;
}

JelaniFMSynthAudioProcessor::~JelaniFMSynthAudioProcessor()
{
	for (int i = 0; i < numRegularParams; i++)
		delete Params[i];
	for (int i = 0; i < kOps; i++)
	{
		for (int j = 0; j < Operator::totalNumParamsPerOperator; j++)
			delete opParams[i][j];
		for (int j = 0; j < kOps; j++)
			delete modParams[i][j];
	}
}

void JelaniFMSynthAudioProcessor::paramReset ()
{
	setParameter (OutLevel, defaultSynthOutLevel);
	setParameter (LFOSpeed, defaultLFOSpeed);
	setParameter (PEAttck, defaultPEAttck);
	setParameter (PEDecay, defaultPEDecay);
	setParameterInternal (NumVoices, (float)defaultNumVoices);
	setParameter (LFOSync, 0.f); lfosync = false;
	setParameterInternal (LFOSyncId, LFO::One4th);
	setParameterInternal (LFOSyncMultId, LFO::Normal);
	setParameter (GlideTime, 0.f);
	for (int i = 0; i < kOps; i++)
	{
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::Activ, (i == kOps - 1) ? 1.f : 0.f);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::WForm, 0);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::Ratio, defaultRatio);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::STOff, defaultSTOff);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::HzOff, defaultHzOff);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::PEAmt, 0.5);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::LFOPi, 0.5);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::Phase, 0);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::Level, defaultLevel);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::VSens, defaultVSens);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::KeySc, defaultKeySc);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::LFOAm, 0.5);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::KTime, 0.5);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::Attck, defaultAttck);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::Decay, defaultDecay);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::Sustn, defaultSustn);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::Relea, defaultRelea);
		setParameter (startOfOpParams + (i * Operator::totalNumParamsPerOperator) + Operator::PostA, (i == kOps - 1) ? 1.f : defaultPostA);
		for (int j = 0; j < kOps; j++)
		{
			setParameter (startOfModParams + (i * kOps) + j, defaultModul);
		}
	}
}

//==============================================================================
const String JelaniFMSynthAudioProcessor::getName() const
{
	return JucePlugin_Name;
}

int JelaniFMSynthAudioProcessor::getNumParameters()
{
	return endOfAllParams;
}

float JelaniFMSynthAudioProcessor::getParameter (int index)
{
	if (index >= 0)
	{
		if (index < numRegularParams)
			return Params[index]->getExtValue ();
		else if (index < startOfModParams)
		{
			int idx = index - startOfOpParams;
			return opParams[idx / Operator::totalNumParamsPerOperator][idx % Operator::totalNumParamsPerOperator]->getExtValue ();
		}
		else if (index < endOfAllParams)
		{
			int idx = index - startOfModParams;
			return modParams[idx / kOps][idx % kOps]->getExtValue ();
		}
		else
			return 0.0f;
	}
	else
		return 0.0f;
}

void JelaniFMSynthAudioProcessor::setParameter (int index, float newValue)
{
	bool report = false;     // report to all voices?
	float reportValue = 0.f;
	if (index >= 0)
	{
		if (index < startOfOpParams)
		{
			Params[index]->setExtValue (newValue);
			if (index == LFOSpeed)
			{
				double sr = getSampleRate();
				if (sr != 0)
					if (index == LFOSpeed) globalLFO.setSpeedInHz (Params[index]->getIntValue(), sr);
			}
			else if (index == LFOSync || index == LFOSyncId || index == LFOSyncMultId)
			{
				Params[index]->setExtValue (newValue);
				lfosync = Params[LFOSync]->getIntValue() > .5;
				if (lfosync)
					globalLFO.setSpeedInBeats (bpm, (int)getParameterInternal (LFOSyncId), (int)getParameterInternal (LFOSyncMultId), getSampleRate());
			}
			else if (index == PEAttck || index == PEDecay)
			{
				reportValue = Params[index]->getIntValue();
				report = true;
			}
			else if (index == NumVoices)
			{
				setNumVoices ((int)(Params[NumVoices]->getIntValue()));
			}
			else if (index == GlideTime)
			{
				reportValue = Params[index]->getIntValue();
				report = true;
			}
		}
		else if (index < startOfModParams)
		{
			int idx = index - startOfOpParams;
			opParams[idx / Operator::totalNumParamsPerOperator][idx % Operator::totalNumParamsPerOperator]->setExtValue (newValue);

			reportValue = opParams[idx / Operator::totalNumParamsPerOperator][idx % Operator::totalNumParamsPerOperator]->getIntValue();
			report = true;
		}
		else if (index < endOfAllParams)
		{
			int idx = index - startOfModParams;
			modParams[idx / kOps][idx % kOps]->setExtValue (newValue);

			reportValue = modParams[idx / kOps][idx % kOps]->getIntValue();
			report = true;
		}

		if (report)
		{
			for (int i = 0; i < getNumVoices(); i++)
				((FMVoice*)synth.getVoice(i))->setParameter(index, reportValue);
		}
		UIUpdateFlag = true;
	}
}

float JelaniFMSynthAudioProcessor::getParameterInternal (int index)
{
	if (index >= 0)
	{
		if (index < numRegularParams)
			return Params[index]->getIntValue ();
		else if (index < startOfModParams)
		{
			int idx = index - startOfOpParams;
			return opParams[idx / Operator::totalNumParamsPerOperator][idx % Operator::totalNumParamsPerOperator]->getIntValue();
		}
		else if (index < endOfAllParams)
		{
			int idx = index - startOfModParams;
			return modParams[idx / kOps][idx % kOps]->getIntValue ();
		}
		else
			return 0.0f;
	}
	else
		return 0.0f;
}

void JelaniFMSynthAudioProcessor::setParameterInternal (int index, float newValue)
{
	if (index >= 0)
	{
		if (index < numRegularParams)
		{
			Params[index]->setIntValue (newValue);
			if (index == NumVoices)
				setNumVoices ((int)newValue);
			else if (index == LFOSync || index == LFOSyncId || index == LFOSyncMultId)
			{
				Params[index]->setIntValue (newValue);
				lfosync = Params[LFOSync]->getIntValue () > .5;
				if (lfosync)
					globalLFO.setSpeedInBeats (bpm, (int)getParameterInternal (LFOSyncId), (int)getParameterInternal (LFOSyncMultId), getSampleRate());
			}
		}
		else if (index < startOfModParams)
		{
			int idx = index - startOfOpParams;
			opParams[idx / Operator::totalNumParamsPerOperator][idx % Operator::totalNumParamsPerOperator]->setIntValue (newValue);
		}
		else if (index < endOfAllParams)
		{
			int idx = index - startOfModParams;
			modParams[idx / kOps][idx % kOps]->setIntValue (newValue);
		}
	}
}

void JelaniFMSynthAudioProcessor::setParameterInternalNotifyingHost (int index, float newValue)
{
	setParameterInternal (index, newValue);
	setParameterNotifyingHost (index, getParameter (index));
}

const String JelaniFMSynthAudioProcessor::getParameterName (int index)
{
	if (index >= 0 && index < endOfAllParams)
	{
		if (index < startOfOpParams)
			return Params[index]->getExtName();
		else if (index < startOfModParams)
			return opParams[(index - startOfOpParams) / Operator::totalNumParamsPerOperator][(index - startOfOpParams) % Operator::totalNumParamsPerOperator]->getExtName();
		else
			return modParams[(index - startOfModParams) / kOps][(index - startOfModParams) % kOps]->getExtName();
		/*if (index < startOfOpParams)
		{
			switch (index)
			{
			case OutLevel:		return "OUT";
			case LFOSpeed:		return "LFOSPD";
			case PEAttck:		return "PEATTCK";
			case PEDecay:		return "PEDECAY";
			case NumVoices:		return "NVOICES";
			case LFOSync:		return "LFOSYNC";
			case LFOSyncId:		return "LFOSYNCSPEED";
			case LFOSyncMultId:	return "LFOSYNCMULTI";
			case GlideTime:		return "GLIDETIME";
			default:			break;
			}
		}
		else if (index < startOfModParams)
		{
			int o = (index - startOfOpParams) / Operator::totalNumParamsPerOperator;
			int p = (index - startOfOpParams) % Operator::totalNumParamsPerOperator;
			return String (Operator::getOperatorName (o)) + " " + Operator::getFriendlyParameterName (p);
		}
		else if (index < endOfAllParams)
		{
			int f = (index - startOfModParams) / kOps;
			int t = (index - startOfModParams) % kOps;
			return "Mod Amt " + String (Operator::getOperatorName (f)) + ">" + Operator::getOperatorName (t);
		}*/
	}

	return String::empty;
}

const String JelaniFMSynthAudioProcessor::getParameterText (int index)
{
	if (index >= 0 && index < endOfAllParams)
	{
		if (index < startOfOpParams)
			return Params[index]->getValue();
		else if (index < startOfModParams)
			return opParams[(index - startOfOpParams) / Operator::totalNumParamsPerOperator][(index - startOfOpParams) % Operator::totalNumParamsPerOperator]->getValue();
		else
			return modParams[(index - startOfModParams) / kOps][(index - startOfModParams) % kOps]->getValue();
		/*if (index < startOfOpParams)
		{
			return Params[index]->getValue() + " (" + Params[index]->getUnit() + ")";
		}
		else if (index < startOfModParams)
		{
			int o = (index - startOfOpParams) / Operator::totalNumParamsPerOperator;
			int p = (index - startOfOpParams) % Operator::totalNumParamsPerOperator;
			return opParams[o][p]->getValue() + " (" + opParams[o][p]->getUnit() + ")";
		}
		else if (index < endOfAllParams)
		{
			int f = (index - startOfModParams) / kOps;
			int t = (index - startOfModParams) % kOps;
			return modParams[f][t]->getValue() + " (" + modParams[f][t]->getUnit() + ")";
		}*/
	}

	return String::empty;
}

const String JelaniFMSynthAudioProcessor::getInputChannelName (int channelIndex) const
{
	return String (channelIndex + 1);
}

const String JelaniFMSynthAudioProcessor::getOutputChannelName (int channelIndex) const
{
	return String (channelIndex + 1);
}

bool JelaniFMSynthAudioProcessor::isInputChannelStereoPair (int /*index*/) const
{
	return true;
}

bool JelaniFMSynthAudioProcessor::isOutputChannelStereoPair (int /*index*/) const
{
	return true;
}

bool JelaniFMSynthAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
	return true;
   #else
	return false;
   #endif
}

bool JelaniFMSynthAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
	return true;
   #else
	return false;
   #endif
}

bool JelaniFMSynthAudioProcessor::silenceInProducesSilenceOut() const
{
	return false;
}

double JelaniFMSynthAudioProcessor::getTailLengthSeconds() const
{
	return juceSynthTailLength;
}

int JelaniFMSynthAudioProcessor::getNumPrograms()
{
	return programCount;
}

int JelaniFMSynthAudioProcessor::getCurrentProgram()
{
	return programLoaded;
}

void JelaniFMSynthAudioProcessor::setCurrentProgram (int index)
{
	if (index >= 0 && index < programCount)
	{
		File pgmfile = File::getSpecialLocation (File::userApplicationDataDirectory).getChildFile ("SineDubio/Jelani/patches/" + programnames[index] + ".xml");
		const String program = pgmfile.loadFileAsString();
		XmlElement* xml = XmlDocument::parse (program);
		//XmlElement* xml = XmlDocument::parse (programs[index]);
		if (xml != nullptr)
		{
			setState (xml);
			programLoaded = index;
		}
		else Logger::outputDebugString ("ERROR parsing XML:\n" + programnames[index]);
		delete xml;
	}
}

const String JelaniFMSynthAudioProcessor::getProgramName (int index)
{
	if (index >= 0 && index < programCount)
		return programnames[index];
	else
		return "Init";
}

void JelaniFMSynthAudioProcessor::prepareToPlay (double sampleRate, int /*samplesPerBlock*/)
{
	if (!lfosync)
		globalLFO.setSpeedInHz (Params[LFOSpeed]->getIntValue (), sampleRate);

	synth.setCurrentPlaybackSampleRate (sampleRate);
	keyboardState.reset();
}

void JelaniFMSynthAudioProcessor::releaseResources()
{
	keyboardState.reset();
}

void JelaniFMSynthAudioProcessor::reset()
{
	keyboardState.reset();
	synth.allNotesOff(0, true);
}

void JelaniFMSynthAudioProcessor::processBlock(AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
	const int numSamples = buffer.getNumSamples();

	// LFO synced? Then check bpm
	// TODO sync to playback beats..
	AudioPlayHead::CurrentPositionInfo newTime;
	if (lfosync && getPlayHead() != nullptr && getPlayHead()->getCurrentPosition (newTime) && bpm != newTime.bpm)
	{
		bpm = newTime.bpm;
		globalLFO.setSpeedInBeats (bpm, (int)getParameterInternal (LFOSyncId), (int)getParameterInternal (LFOSyncMultId), getSampleRate ());
	}
	// fix LFO buffer for numSamples
	if (LFOBufInitialised) delete LFOBuf;
	LFOBuf = new double[numSamples];
	for (int i = 0; i < numSamples; i++)
		*(LFOBuf+i) = globalLFO.getNextValue();
	LFOBufInitialised = true;

	for (int i = 0; i < getNumVoices(); i++)
		((FMVoice*)synth.getVoice (i))->setLFOBuffer (LFOBuf, numSamples);

	keyboardState.processNextMidiBuffer (midiMessages, 0, numSamples, true);

	synth.renderNextBlock (buffer, midiMessages, 0, numSamples);

	buffer.applyGain (((DecibelParameter*)Params[OutLevel])->getLinearAmp());
}

AudioProcessorEditor* JelaniFMSynthAudioProcessor::createEditor()
{
	return new JelaniFMSynthAudioProcessorEditor (this);
}

void JelaniFMSynthAudioProcessor::getStateInformation (MemoryBlock& destData)
{
	copyXmlToBinary (*getState(), destData);
}

XmlElement* JelaniFMSynthAudioProcessor::getState()
{
	XmlElement* xml = new XmlElement ("WTSYNTHPLUGINSETTINGS");
	for (int pm = 0; pm < numRegularParams; pm++)
		xml->setAttribute (getParameterName (pm), Params[pm]->getExtValue());
	xml->setAttribute ("OPS", kOps);
	for (int op = 0; op < kOps; op++)
	{
		XmlElement* opXml = new XmlElement ("OP" + String (op));
		for (int pr = 0; pr < Operator::totalNumParamsPerOperator; pr++)
			opXml->setAttribute (Operator::getParameterName (pr), opParams[op][pr]->getExtValue());
		for (int dest = 0; dest < kOps; dest++)
			opXml->setAttribute ("MOD" + String (dest), modParams[op][dest]->getExtValue());
		xml->addChildElement (opXml);
	}
	return xml;
}

void JelaniFMSynthAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
	ScopedPointer<XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));
	if (xmlState != nullptr)
		setState (xmlState);
}

void JelaniFMSynthAudioProcessor::setState (XmlElement* xml)
{
	paramReset();
	if (xml->hasTagName ("WTSYNTHPLUGINSETTINGS"))
	{
		bool interpretAsOldEnvelopeTimes = xml->hasAttribute ("INTERPRET");
		for (int i = 0; i < numRegularParams; i++)
			if (xml->hasAttribute (getParameterName (i)))
				setParameterNotifyingHost (i, (float)xml->getDoubleAttribute (getParameterName (i)));
		int opsInXml = xml->getIntAttribute ("OPS");
		for (int i = 0; i < opsInXml && i < kOps; i++)
		{
			XmlElement* opXml (xml->getChildByName ("OP" + String (i)));
			for (int pr = 0; pr < Operator::totalNumParamsPerOperator; pr++)
			{
				String opName = Operator::getParameterName (pr);
				if (opXml->hasAttribute (opName))
				{
					if (interpretAsOldEnvelopeTimes)
					{
						if (opName == "ATTCK" || opName == "DECAY")
						{
							int nr = JelaniFMSynthAudioProcessor::startOfOpParams + i * Operator::totalNumParamsPerOperator + pr;
							setParameter (nr, (float)opXml->getDoubleAttribute (opName));
							setParameterInternalNotifyingHost (nr, getParameterInternal (nr) / 10.f);
						}
						else if (opName == "RELEA")
						{
							int nr = JelaniFMSynthAudioProcessor::startOfOpParams + i * Operator::totalNumParamsPerOperator + pr;
							setParameter (nr, (float)opXml->getDoubleAttribute (opName));
							setParameterInternalNotifyingHost (nr, getParameterInternal (nr) / 3.f);
						}
						else
							setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams + i * Operator::totalNumParamsPerOperator + pr, (float)opXml->getDoubleAttribute (opName));
					}
					else
						setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams + i * Operator::totalNumParamsPerOperator + pr, (float)opXml->getDoubleAttribute (opName));
				}
			}
			for (int dest = 0; dest < opsInXml && dest < kOps; dest++)
			{
				String destName = "MOD" + String (dest);
				if (opXml->hasAttribute (destName))
					setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfModParams + i * kOps + dest, (float)opXml->getDoubleAttribute (destName));
			}
		}
	}
	UIUpdateFlag = true;
}

//==============================================================================
WaveTable* JelaniFMSynthAudioProcessor::getWT (int w)
{
	if (w < kWaveForms)
		return WaveTables + w;
	else
		return nullptr;
}

void JelaniFMSynthAudioProcessor::setNumVoices (int nv)
{
	int current = getNumVoices();
	if (nv < current)
	{
		for (int i = current; i > nv; i--)
		{
			synth.removeVoice (0);
		}
		if (nv == 1)
		{
			glide = true;
			((FMVoice*)synth.getVoice (0))->setParameter (GlideTime, Params[GlideTime]->getIntValue());
		}
	}
	else if (nv > current)
	{
		if (current == 1)
		{
			glide = false;
			((FMVoice*)synth.getVoice (0))->setParameter (GlideTime, 0.f);
		}
		for (int i = current; i < nv; i++)
		{
			addNewVoiceToSynth();
		}
	}
}

void JelaniFMSynthAudioProcessor::addNewVoiceToSynth ()
{
	FMVoice* voiceOfAGeneration = new FMVoice (WaveTables);
	// propagate all parameters to this voice
	for (int i = 0; i < numRegularParams; i++)
	{
		if (i == PEAttck || i == PEDecay)
			voiceOfAGeneration->setParameter (i, Params[i]->getIntValue());
	}
	for (int i = startOfOpParams; i < startOfModParams; i++)
	{
		int idx = i - startOfOpParams;
		voiceOfAGeneration->setParameter (i, opParams[idx / Operator::totalNumParamsPerOperator][idx % Operator::totalNumParamsPerOperator]->getIntValue());
	}
	for (int i = startOfModParams; i < endOfAllParams; i++)
	{
		int idx = i - startOfModParams;
		voiceOfAGeneration->setParameter (i, modParams[idx / kOps][idx % kOps]->getIntValue());
	}
	synth.addVoice (voiceOfAGeneration);
}

double JelaniFMSynthAudioProcessor::getLFOSpeed()
{
	return globalLFO.getSpeedInHz (getSampleRate());
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
	return new JelaniFMSynthAudioProcessor();
}
