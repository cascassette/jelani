/*
  ==============================================================================

	FMVoice.h
	Created: 22 Jan 2014 3:44:30pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef FMVOICE_H_INCLUDED
#define FMVOICE_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "WaveTable.h"
#include "Operator.h"
#include "AD.h"
#include "const.h"

// FM (wavetable) Synth Sound
class FMSound : public SynthesiserSound
{
public:
	FMSound() { }

	bool appliesToNote(const int /*midiNoteNumber*/)	{ return true; }
	bool appliesToChannel(const int /*midiChannel*/)	{ return true; }
};

// FM (wavetable) Synth Voice
class FMVoice : public SynthesiserVoice
{
public:
	// Inits operators
	FMVoice(WaveTable* wavtab);

	// Info for synth - in this case, just say yes
	bool canPlaySound(SynthesiserSound* sound);

	// Relays setParameter messages from Processor to Operators
	void setParameter (int index, float newValue);	// NB. internal value already!

	// Just relays midi note info to operators
	void startNote (int midiNoteNumber, float velocity,
					SynthesiserSound* sound, int currentPitchWheelPosition);
    int stopNote (bool allowTailOff);
    void stopNote (float velocity, bool allowTailOff);

	// Takes new pointer to read LFO values from
	void setLFOBuffer (double* newLFOBuffer, int numSamples);

	// Checks if envelope is still positive and gets a lot of NextSamples
	void renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples);
	// Cross-modulates outputs from operators according to mod matrix
	// and mixes the signals according to their out levels
	double getNextSample (double lfoVal);

	// Not implemented yet
	void pitchWheelMoved (int /*newValue*/) { }
	void controllerMoved (int /*controllerNumber*/, int /*newValue*/) { }

private:
	// Keeps y' for every operator
	double lastSample[kOps];
	// True if every operator's: (env is done || postamp is zero)
	bool isEnvDone();

	// Operators that will keep status of op parameters
	// and provide for wavetable lookup
	Operator operators[kOps];
	// Besides in main param list mod amounts get stored here, ready, 'intern value' ie. in gain amount
	float mod[kOps][kOps];

	// Stores the next numSamples of LFO values for renderNextBlock
	double* LFOBuffer;

	// Points to the first wavetable
	//WaveTable* wt;

	// Pitch envelope
	AD pitchEnv;

	// Pitch glide helper envelope
	AD glideEnv;
	int glideDelta;
	int lastPlayingNote;
};

#endif	// FMVOICE_H_INCLUDED
