/*
  ==============================================================================

	LFO.h
	Created: 14 Feb 2014 11:13:23pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef LFO_H_INCLUDED
#define LFO_H_INCLUDED
#include "../JuceLibraryCode/JuceHeader.h"

class LFO
{
public:
	// Init
	LFO();
	// Re-init
	void reset () { phase = 0.0; }

	// Set speed
	void setSpeedInHz (double speed, double sampleRate);
	void setSpeedInBeats (double bpm, int syncId, int syncMultId, double sampleRate);
	// Get speed
	double getSpeedInHz(double sampleRate);

	// Get value
	double getNextValue ();
	double getCurrentValue ();

	enum LFOSyncIds
	{
		Four = 0,
		Two,
		One,
		One2nd,
		One4th,
		One8th,
		One16th,
		One32nd,

		numLFOSyncIds
	};

	enum LFOSyncMultIds
	{
		Dotted = 0,
		Normal,
		Triplet,

		numLFOSyncMultIds
	};

	static const String getSyncSpeedName (int syncId, int syncMultId);

private:
	double phase;
	double phaseDelta;

	double LFOSyncFreqMultValues[numLFOSyncMultIds];
};



#endif	// LFO_H_INCLUDED
