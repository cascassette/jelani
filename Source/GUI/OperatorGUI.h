/*
  ==============================================================================

	OperatorGUI.h
	Created: 23 Jan 2014 7:15:18pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef OPERATORGUI_H_INCLUDED
#define OPERATORGUI_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "WFDisplay.h"
#include "../WaveTable.h"
#include "LookAndFeel/JelaniLookAndFeel.h"
#include "LookAndFeel/JelaniRotarySliderMid.h"
#include "LookAndFeel/JelaniRotarySliderRight.h"
#include "LookAndFeel/JelaniRotarySliderNumber.h"

class OperatorGUI	 : public Component,
					   public SliderListener,
					   public ChangeBroadcaster,
					   public ButtonListener,
					   public LabelListener
{
public:
	// Constructor, needs to know his place in line
	OperatorGUI (int idx, WaveTable* wavtab);
	~OperatorGUI();

	int getIdx() { return id; };

	// Background is a shade of grey according to idx
	void paint (Graphics&);
	// Arranges, makes stuff fit
	void resized();

	// Helper class to take care of this component's special needs
	class Listener
	{
	public:
		virtual ~Listener() {};
		virtual void operatorParameterChanged (int oIdx, int pIdx, float newValue) = 0;
		virtual void modParameterChanged (int fromIdx, int toIdx, float newValue) = 0;
		virtual void copyEnvelopeBuffer (int fromIdx, float *ADSR) = 0;
		virtual void pasteEnvelopeBuffer (int toIdx) = 0;
	};
	// Copied these from juces Slider sort of
	void addListener(Listener* listener);
	void removeListener(Listener* listener);

	// Callbacks that just relay to OperatorGUI::Listener objects
	// ie JelaniFMSynthAudioProcessorEditor
	void sliderValueChanged (Slider* slider);
	void labelTextChanged (Label* label);
	void buttonClicked (Button* button);
	void buttonStateChanged (Button* button);

	// For when host sets values directly, instead of the user via the GUI
	void setOpValue (int pIdx, double newValue, juce::NotificationType notification);
	void setModValue (int toIdx, double newValue, juce::NotificationType notification);

	// Set whole envelope from other OperatorGUI at once
	void setADSR (float* ADSR);

	// Tell the WFDisplay unit to display another WaveTable
	void setWT (int w);

private:
	int id;
	int totalWidth;
	Colour clrBG;

	WaveTable* wt;

	bool activ;
	void updateActiv();

	ScopedPointer<ToggleButton> tglOpIdx;
	ScopedPointer<WFDisplay>    sldWForm;
	ScopedPointer<Label>        txtRatio;
	ScopedPointer<Label>        txtSTOff;
	ScopedPointer<Label>        txtHzOff;
	ScopedPointer<Slider>       sldLFOPi;
	ScopedPointer<Slider>       sldPEAmt;
	ScopedPointer<Slider>       sldPhase;
	ScopedPointer<Slider>       sldLevel;
	ScopedPointer<Slider>       sldVSens;
	ScopedPointer<Slider>       sldKeySc;
	ScopedPointer<Slider>       sldLFOAm;
	ScopedPointer<Slider>       sldKTime;
	ScopedPointer<Slider>       sldAttck;
	ScopedPointer<Slider>       sldDecay;
	ScopedPointer<Slider>       sldSustn;
	ScopedPointer<Slider>       sldRelea;
	ScopedPointer<ImageButton>  btnCpEnv;
	ScopedPointer<ImageButton>  btnPtEnv;
	OwnedArray<Slider>	        sldArrayMod;
	ScopedPointer<Slider>       sldPostA;

	ListenerList<OperatorGUI::Listener> listeners;

	ScopedPointer<JelaniLookAndFeel> laf;
	ScopedPointer<JelaniRotarySliderMid> laf_mid;
	ScopedPointer<JelaniRotarySliderRight> laf_right;
	//ScopedPointer<JelaniRotarySliderNumber> laf_num;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (OperatorGUI)
};

typedef OperatorGUI::Listener OperatorGUIListener;

#endif	// OPERATORGUI_H_INCLUDED
