/*
  ==============================================================================

	OperatorGUI.cpp
	Created: 23 Jan 2014 7:15:18pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "OperatorGUI.h"
#include "../Operator.h"
#include "../const.h"

//==============================================================================
OperatorGUI::OperatorGUI(int idx, WaveTable* wavtab)
{
	id = idx;
	wt = wavtab;

	laf = new JelaniLookAndFeel();
	laf_mid = new JelaniRotarySliderMid();
	//laf_right = new JelaniRotarySliderRight();
	//laf_num = new JelaniRotarySliderNumber();

	addAndMakeVisible (tglOpIdx = new ToggleButton (Operator::getOperatorName(id)));
	tglOpIdx->addListener (this);

	addAndMakeVisible (sldWForm = new WFDisplay (clrBright, clrDark));
	sldWForm->setTooltip ("Waveform " + String (Operator::getOperatorName (idx)));
	sldWForm->setRange (0, 1, 1./kWaveForms);
	sldWForm->setSliderStyle (Slider::RotaryVerticalDrag);
	sldWForm->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldWForm->setDoubleClickReturnValue (true, 0);
	sldWForm->addListener (this);
	setWT (0);

	addAndMakeVisible (txtRatio = new Label ("newdexdedudu"));
	txtRatio->setJustificationType (Justification::centredRight);
	txtRatio->setTooltip ("Ratio " + String (Operator::getOperatorName (idx)));
	txtRatio->setColour (Label::backgroundColourId, Colour (0xffffffff));
	txtRatio->setColour (Label::textColourId, Colour (0xff000000));
	txtRatio->setEditable (false, true, false);
	txtRatio->addListener (this);

	addAndMakeVisible (txtSTOff = new Label ("new texteditor"));
	txtSTOff->setJustificationType (Justification::centredRight);
	txtSTOff->setTooltip ("Semitone offset " + String (Operator::getOperatorName (idx)));
	txtSTOff->setColour (Label::backgroundColourId, Colour (0xffffffff));
	txtSTOff->setColour (Label::textColourId, Colour (0xff000000));
	txtSTOff->setEditable (false, true, false);
	txtSTOff->addListener (this);

	addAndMakeVisible (txtHzOff = new Label ("new texteditor"));
	txtHzOff->setJustificationType (Justification::centredRight);
	txtHzOff->setTooltip ("Linear pitch offset " + String (Operator::getOperatorName (idx)));
	txtHzOff->setColour (Label::backgroundColourId, Colour (0xffffffff));
	txtHzOff->setColour (Label::textColourId, Colour (0xff000000));
	txtHzOff->setEditable (false, true, false);
	txtHzOff->addListener (this);

	addAndMakeVisible (sldLFOPi = new Slider ("new slider"));
	sldLFOPi->setTooltip ("LFO>Pitch " + String (Operator::getOperatorName (idx)));
	sldLFOPi->setRange (0, 1, 0);
	sldLFOPi->setSliderStyle (Slider::RotaryVerticalDrag);
	sldLFOPi->setLookAndFeel (laf_mid);
	sldLFOPi->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldLFOPi->setDoubleClickReturnValue (true, 0.5);
	sldLFOPi->addListener (this);

	addAndMakeVisible (sldPEAmt = new Slider ("new slider"));
	sldPEAmt->setTooltip ("Pitch Env Amount " + String (Operator::getOperatorName (idx)));
	sldPEAmt->setRange (0, 1, 0);
	sldPEAmt->setSliderStyle (Slider::RotaryVerticalDrag);
	sldPEAmt->setLookAndFeel (laf_mid);
	sldPEAmt->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldPEAmt->setDoubleClickReturnValue (true, .5);
	sldPEAmt->addListener (this);

	addAndMakeVisible (sldPhase = new Slider ("new slider"));
	sldPhase->setTooltip ("Phase " + String (Operator::getOperatorName (idx)));
	sldPhase->setRange (0, 1, 0);
	sldPhase->setSliderStyle (Slider::RotaryVerticalDrag);
	sldPhase->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldPhase->setDoubleClickReturnValue (true, 0);
	sldPhase->addListener (this);

	addAndMakeVisible (sldLevel = new Slider ("new slider"));
	sldLevel->setTooltip ("Level " + String (Operator::getOperatorName (idx)));
	sldLevel->setRange (0, 1, 0);
	sldLevel->setSliderStyle (Slider::RotaryVerticalDrag);
	sldLevel->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldLevel->setDoubleClickReturnValue (true, defaultLevel);
	sldLevel->addListener (this);

	addAndMakeVisible (sldVSens = new Slider ("new slider"));
	sldVSens->setTooltip ("Vel. Sens " + String (Operator::getOperatorName (idx)));
	sldVSens->setRange (0, 1, 0);
	sldVSens->setSliderStyle (Slider::RotaryVerticalDrag);
	//sldVSens->setLookAndFeel (laf_right);
	sldVSens->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldVSens->setSkewFactor (defaultVSensSkew);
	sldVSens->setDoubleClickReturnValue (true, defaultVSens);
	sldVSens->addListener (this);

	addAndMakeVisible (sldKeySc = new Slider ("new slider"));
	sldKeySc->setTooltip ("Key Scaling " + String (Operator::getOperatorName (idx)));
	sldKeySc->setRange (0, 1, 0);
	sldKeySc->setSliderStyle (Slider::RotaryVerticalDrag);
	sldKeySc->setLookAndFeel (laf_mid);
	sldKeySc->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldKeySc->setDoubleClickReturnValue (true, 0.5);
	sldKeySc->addListener (this);

	addAndMakeVisible (sldLFOAm = new Slider ("new slider"));
	sldLFOAm->setTooltip ("LFO>Amp " + String (Operator::getOperatorName (idx)));
	sldLFOAm->setRange (0, 1, 0);
	sldLFOAm->setSliderStyle (Slider::RotaryVerticalDrag);
	sldLFOAm->setLookAndFeel (laf_mid);
	sldLFOAm->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldLFOAm->setDoubleClickReturnValue (true, 0.5);
	sldLFOAm->addListener (this);

	addAndMakeVisible (sldKTime = new Slider ("new slider"));
	sldKTime->setTooltip ("Key>Env time " + String (Operator::getOperatorName (idx)));
	sldKTime->setRange (0, 1, 0);
	sldKTime->setSliderStyle (Slider::RotaryVerticalDrag);
	sldKTime->setLookAndFeel (laf_mid);
	sldKTime->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldKTime->setDoubleClickReturnValue (true, 0.5);
	sldKTime->addListener (this);

	addAndMakeVisible (sldAttck = new Slider ("new slider"));
	sldAttck->setTooltip ("Attack");
	sldAttck->setRange (0, 1, 0);
	sldAttck->setSliderStyle (Slider::RotaryVerticalDrag);
	sldAttck->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldAttck->setSkewFactor (defaultEnvTimeSkew);
	sldAttck->setDoubleClickReturnValue (true, defaultAttck);
	sldAttck->addListener (this);

	addAndMakeVisible (sldDecay = new Slider ("new slider"));
	sldDecay->setTooltip ("Decay");
	sldDecay->setRange (0, 1, 0);
	sldDecay->setSliderStyle (Slider::RotaryVerticalDrag);
	sldDecay->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldDecay->setSkewFactor (defaultEnvTimeSkew);
	sldDecay->setDoubleClickReturnValue (true, defaultDecay);
	sldDecay->addListener (this);

	addAndMakeVisible (sldSustn = new Slider ("new slider"));
	sldSustn->setTooltip ("Sustain");
	sldSustn->setRange (0, 1, 0);
	sldSustn->setSliderStyle (Slider::RotaryVerticalDrag);
	sldSustn->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldSustn->setDoubleClickReturnValue (true, defaultSustn);
	sldSustn->addListener (this);

	addAndMakeVisible (sldRelea = new Slider ("new slider"));
	sldRelea->setTooltip ("Release");
	sldRelea->setRange (0, 1, 0);
	sldRelea->setSliderStyle (Slider::RotaryVerticalDrag);
	sldRelea->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldRelea->setSkewFactor (defaultEnvTimeSkew);
	sldRelea->setDoubleClickReturnValue (true, defaultRelea);
	sldRelea->addListener (this);

	Image tempImage;

	addAndMakeVisible (btnCpEnv = new ImageButton ("C"));
	btnCpEnv->setTooltip ("Copy envelope");
	tempImage = ImageCache::getFromMemory (BinaryData::copyblue_png, BinaryData::copyblue_pngSize);
	btnCpEnv->setImages (false, true, true,
	                     tempImage, .8f, Colours::transparentBlack,
						 tempImage, 1.f, Colour (0x30ffffff),
						 tempImage, 1.f, Colour (0x60ffffff),
						 0.5f);
	btnCpEnv->addListener (this);

	addAndMakeVisible (btnPtEnv = new ImageButton ("P"));
	btnPtEnv->setTooltip ("Paste envelope");
	tempImage = ImageCache::getFromMemory (BinaryData::pasteblue_png, BinaryData::pasteblue_pngSize);
	btnPtEnv->setImages (false, true, true,
	                     tempImage, .8f, Colours::transparentBlack,
						 tempImage, 1.f, Colour (0x30ffffff),
						 tempImage, 1.f, Colour (0x60ffffff),
						 0.5f);
	btnPtEnv->addListener (this);

	totalWidth = 804;
	for (int i = 0; i < kOps; i++)
	{
		sldArrayMod.add (new Slider ("sldMod" + String (Operator::getOperatorName (idx)) + "to" + String (i)));
		addAndMakeVisible (sldArrayMod.getLast ());
		sldArrayMod.getLast()->setTooltip (String (Operator::getOperatorName (idx)) + "->" + String (Operator::getOperatorName (i)));
		sldArrayMod.getLast()->setRange (0, 1, 0);
		sldArrayMod.getLast()->setSliderStyle (Slider::RotaryVerticalDrag);
		sldArrayMod.getLast()->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
		sldArrayMod.getLast ()->setSkewFactor (2.);
		//sldArrayMod.getLast()->setLookAndFeel (laf_num);
		sldArrayMod.getLast()->setDoubleClickReturnValue (true, .75);
		sldArrayMod.getLast()->addListener (this);
		// if (i < (idx - 1)) sldArrayMod.getLast()->setVisible (false);
		totalWidth += 32;
	}

	addAndMakeVisible (sldPostA = new Slider ("new slider"));
	sldPostA->setTooltip (String (Operator::getOperatorName (idx)) + "->Out");
	sldPostA->setRange (0, 1, 0);
	sldPostA->setSliderStyle (Slider::RotaryVerticalDrag);
	sldPostA->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldPostA->setDoubleClickReturnValue (true, 1);
	sldPostA->addListener (this);
	totalWidth += 32;

	// Background colour, think about it!
	float bLevel = (float)(id + 1) / (float)(kOps + 1);
	bLevel = .3f + .5f * bLevel;
	clrBG = Colour::fromFloatRGBA (bLevel, bLevel, bLevel, 1);

	setSize(totalWidth, 32);
}

OperatorGUI::~OperatorGUI()
{
	sldWForm = nullptr;
	txtRatio = nullptr;
	txtSTOff = nullptr;
	txtHzOff = nullptr;
	sldLFOPi = nullptr;
	sldPEAmt = nullptr;
	sldPhase = nullptr;
	sldLevel = nullptr;
	sldVSens = nullptr;
	sldKeySc = nullptr;
	sldLFOAm = nullptr;
	sldKTime = nullptr;
	sldAttck = nullptr;
	sldDecay = nullptr;
	sldSustn = nullptr;
	sldRelea = nullptr;
	sldPostA = nullptr;
	sldArrayMod.clear();
}

void OperatorGUI::paint (Graphics& g)
{
	g.setColour (activ ? clrBG : clrDark);
	g.fillRoundedRectangle (1.0f, 1.0f, getWidth() - 2.0f, getHeight() - 2.0f, 3.f);

	// section delimiters
	g.setColour (Colours::black);
	// Operator label section
	int x = 0;
	int dx = 86;
	g.drawRect (x, 0, dx, 32);
	// Pitch section
	x += dx;
	dx = 278;
	g.drawRect (x, 0, dx, 32);
	// Level section
	x += dx;
	dx = 128;
	g.drawRect (x, 0, dx, 32);
	// Envelope section
	x += dx;
	dx = 212;
	g.drawRect (x, 0, dx, 32);
	// Mod section
	x += dx;
	dx = kOps * 32;
	g.drawRect (x, 0, dx, 32);
	// Post Amp section
	x += dx;
	dx = 32;
	g.drawRect (x, 0, dx, 32);
}

void OperatorGUI::resized()
{
	int x = 4; // 4 is the margin for all the contained components, like
			   // rotaries that have w=h=24
	tglOpIdx->setBounds (x, 4, 24, 24); x += 30;
	sldWForm->setBounds (x, 4, 48, 24); x += 56;
	txtRatio->setBounds (x, 4, 56, 24); x += 60;
	txtSTOff->setBounds (x, 4, 56, 24); x += 60;
	txtHzOff->setBounds (x, 4, 56, 24); x += 62;
	sldLFOPi->setBounds (x, 4, 24, 24); x += 32;
	sldPEAmt->setBounds (x, 4, 24, 24); x += 32;
	sldPhase->setBounds (x, 4, 24, 24); x += 32;
	sldLevel->setBounds (x, 4, 24, 24); x += 32;
	sldVSens->setBounds (x, 4, 24, 24); x += 32;
	sldKeySc->setBounds (x, 4, 24, 24); x += 32;
	sldLFOAm->setBounds (x, 4, 24, 24); x += 32;
	sldKTime->setBounds (x, 4, 24, 24); x += 32;
	sldAttck->setBounds (x, 4, 24, 24); x += 32;
	sldDecay->setBounds (x, 4, 24, 24); x += 32;
	sldSustn->setBounds (x, 4, 24, 24); x += 32;
	sldRelea->setBounds (x, 4, 24, 24); x += 28;
	btnCpEnv->setBounds (x, 4, 24, 24); x += 24;
	btnPtEnv->setBounds (x, 4, 24, 24); x += 32;
	for (int i = 0; i < kOps; i++)
	{
		sldArrayMod[i]->setBounds (x, 4, 24, 24);
		x += 32;
	}
	sldPostA->setBounds (x, 4, 24, 24);
}

void OperatorGUI::sliderValueChanged (Slider* slider)
{
	if (slider == sldWForm)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::WForm, (float)slider->getValue());

	else if (slider == sldLFOPi)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::LFOPi, (float)slider->getValue());

	else if (slider == sldPEAmt)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::PEAmt, (float)slider->getValue());

	else if (slider == sldPhase)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::Phase, (float)slider->getValue());

	else if (slider == sldLevel)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::Level, (float)slider->getValue());

	else if (slider == sldVSens)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::VSens, (float)slider->getValue());

	else if (slider == sldKeySc)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::KeySc, (float)slider->getValue());

	else if (slider == sldLFOAm)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::LFOAm, (float)slider->getValue());

	else if (slider == sldKTime)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::KTime, (float)slider->getValue());

	else if (slider == sldAttck)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::Attck, (float)slider->getValue());

	else if (slider == sldDecay)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::Decay, (float)slider->getValue());

	else if (slider == sldSustn)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::Sustn, (float)slider->getValue());

	else if (slider == sldRelea)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::Relea, (float)slider->getValue());

	else if (slider == sldPostA)
	{
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::PostA, (float)slider->getValue());
		if (slider->getValue() == 1) slider->setDoubleClickReturnValue (true, 0);
		else if (slider->getValue() == 0) slider->setDoubleClickReturnValue (true, 1);
	}

	else // slider in mod array; no need to check for slider to be in sldArrayMod as long as all regular parameter sliders are checked above
	{
		juce::String name = slider->getName();
		int mod = name.substring (name.length() - 1).getIntValue();
		listeners.call (&OperatorGUI::Listener::modParameterChanged, id, mod, (float)slider->getValue());
		if (slider->getValue() >= 0.75) slider->setDoubleClickReturnValue (true, 0);
		else if (slider->getValue() == 0) slider->setDoubleClickReturnValue (true, .75);
	}
}

void OperatorGUI::labelTextChanged (Label* label)
{
	if (label == txtRatio)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::Ratio, txtRatio->getText().getFloatValue());
	else if (label == txtSTOff)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::STOff, txtSTOff->getText().getFloatValue());
	else if (label == txtHzOff)
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::HzOff, txtHzOff->getText().getFloatValue());
}

void OperatorGUI::buttonClicked (Button* button)
{
	if (button == btnCpEnv)
	{
		float buf[ADSR::totalNumEnvPhases];
		buf[ADSR::Attack] = (float)sldAttck->getValue ();
		buf[ADSR::Decay] = (float)sldDecay->getValue ();
		buf[ADSR::Sustain] = (float)sldSustn->getValue ();
		buf[ADSR::Release] = (float)sldRelea->getValue ();
		listeners.call (&OperatorGUI::Listener::copyEnvelopeBuffer, id, buf);
	}
	else if (button == btnPtEnv)
		listeners.call (&OperatorGUI::Listener::pasteEnvelopeBuffer, id);
}

void OperatorGUI::buttonStateChanged (Button* button)
{
	if (button == tglOpIdx && tglOpIdx->getToggleState() != activ)
	{
		activ = tglOpIdx->getToggleState();
		listeners.call (&OperatorGUI::Listener::operatorParameterChanged, id, Operator::Activ, activ ? 1.f : 0.f);

		updateActiv ();
	}
}

void OperatorGUI::setOpValue(int pIdx, double newValue, juce::NotificationType notification)
{
	switch (pIdx)
	{
	case Operator::Activ:	activ = newValue > 0.5; updateActiv(); break;
	case Operator::WForm:	sldWForm->setValue (newValue, notification); break;
	case Operator::Ratio:	txtRatio->setText (String (newValue, 4), notification); break;
	case Operator::STOff:	txtSTOff->setText (String (newValue, 4), notification); break;
	case Operator::HzOff:	txtHzOff->setText (String (newValue, 4), notification); break;
	case Operator::LFOPi:	sldLFOPi->setValue (newValue, notification); break;
	case Operator::PEAmt:	sldPEAmt->setValue (newValue, notification); break;
	case Operator::Phase:	sldPhase->setValue (newValue, notification); break;
	case Operator::Level:	sldLevel->setValue (newValue, notification); break;
	case Operator::VSens:	sldVSens->setValue (newValue, notification); break;
	case Operator::KeySc:	sldKeySc->setValue (newValue, notification); break;
	case Operator::LFOAm:	sldLFOAm->setValue (newValue, notification); break;
	case Operator::KTime:	sldKTime->setValue (newValue, notification); break;
	case Operator::Attck:	sldAttck->setValue (newValue, notification); break;
	case Operator::Decay:	sldDecay->setValue (newValue, notification); break;
	case Operator::Sustn:	sldSustn->setValue (newValue, notification); break;
	case Operator::Relea:	sldRelea->setValue (newValue, notification); break;
	case Operator::PostA:	sldPostA->setValue (newValue, notification); break;
	}
}

void OperatorGUI::setModValue(int toIdx, double newValue, juce::NotificationType notification)
{
	if (toIdx >= 0 && toIdx < kOps)
		sldArrayMod[toIdx]->setValue(newValue, notification);
}

void OperatorGUI::setADSR (float* ADSR)
{
	sldAttck->setValue (ADSR[ADSR::Attack], dontSendNotification);
	sldDecay->setValue (ADSR[ADSR::Decay], dontSendNotification);
	sldSustn->setValue (ADSR[ADSR::Sustain], dontSendNotification);
	sldRelea->setValue (ADSR[ADSR::Release], dontSendNotification);
}

void OperatorGUI::setWT (int w)
{
	sldWForm->setWT (wt + w);
	sldWForm->setTooltip (Operator::getFriendlyWFName (w));
}

void OperatorGUI::updateActiv()
{
	tglOpIdx->setToggleState (activ, dontSendNotification);
	sldWForm->setFG (activ ? clrBright : clrDim);
	Colour clrSliderFill = activ ? clrJelaniBlue : Colours::whitesmoke.darker (.6f);
	sldLFOPi->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldPEAmt->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldPhase->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldLevel->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldVSens->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldKeySc->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldLFOAm->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldKTime->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldAttck->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldDecay->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldSustn->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	sldRelea->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	btnCpEnv->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	btnPtEnv->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	for (int i = 0; i < kOps; i++)
	{
		sldArrayMod[i]->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	}
	sldPostA->setColour (Slider::rotarySliderFillColourId, clrSliderFill);
	repaint ();
}

void OperatorGUI::addListener(OperatorGUI::Listener* const listener) { listeners.add(listener); }
void OperatorGUI::removeListener (OperatorGUI::Listener* const listener) { listeners.remove (listener); }
