/*
  ==============================================================================

    DottedTripletSelector.cpp
    Created: 8 Mar 2014 9:51:30pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "DottedTripletSelector.h"

DottedTripletSelector::DottedTripletSelector()
{
	imgTriplet = ImageCache::getFromMemory (BinaryData::threehalves_png, BinaryData::threehalves_pngSize);
	imgDotted = ImageCache::getFromMemory (BinaryData::twothirds_png, BinaryData::twothirds_pngSize);
	setSize (24, 24);
	SMID = LFO::Normal;
	rctTriplet = Rectangle<int> (0, 3, 6, 6);
	rctDotted = Rectangle<int> (0, 15, 6, 6);
}

DottedTripletSelector::~DottedTripletSelector()
{
}

void DottedTripletSelector::paint (Graphics& g)
{
	if (syncEnabled)
	{
		if (SMID == LFO::Triplet)
			g.setColour (findColour (TextButton::buttonOnColourId));
		else
			g.setColour (findColour (TextButton::buttonColourId));
		g.fillRect (rctTriplet);

		if (SMID == LFO::Dotted)
			g.setColour (findColour (TextButton::buttonOnColourId));
		else
			g.setColour (findColour (TextButton::buttonColourId));
		g.fillRect (rctDotted);

		g.drawImageAt (imgTriplet, 8, 0);
		g.drawImageAt (imgDotted, 8, 12);
	}
}

void DottedTripletSelector::resized()
{
}

LFO::LFOSyncMultIds DottedTripletSelector::getSMID()
{
	return SMID;
}

void DottedTripletSelector::setSMID (LFO::LFOSyncMultIds newSMID)
{
	SMID = newSMID;
	repaint();
}

void DottedTripletSelector::mouseUp (const MouseEvent& e)
{
	if (rctTriplet.contains (e.getPosition()))
	{
		if (SMID == LFO::Triplet)
			SMID = LFO::Normal;
		else
			SMID = LFO::Triplet;
		listeners.call (&DottedTripletSelector::Listener::syncMultIdChanged, SMID);
		repaint();
	}
	else if (rctDotted.contains (e.getPosition()))
	{
		if (SMID == LFO::Dotted)
			SMID = LFO::Normal;
		else
			SMID = LFO::Dotted;
		listeners.call (&DottedTripletSelector::Listener::syncMultIdChanged, SMID);
		repaint();
	}
}

void DottedTripletSelector::setSyncEnabled (bool well)
{
	syncEnabled = well;
	repaint();
}

void DottedTripletSelector::addListener (DottedTripletSelector::Listener* const listener) { listeners.add (listener); }
void DottedTripletSelector::removeListener (DottedTripletSelector::Listener* const listener) { listeners.remove (listener); }