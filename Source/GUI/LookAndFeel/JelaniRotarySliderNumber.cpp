/*
  ==============================================================================

    JelaniRotarySliderNumber.cpp
    Created: 13 Mar 2014 1:51:50pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "JelaniRotarySliderNumber.h"

void JelaniRotarySliderNumber::drawRotarySlider (Graphics& g, int x, int y, int width, int height, float sliderPos,
	const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider)
{
	int number;
	{
		const double value = slider.getValue ();
		const double maximum = slider.getMaximum ();
		const double minimum = slider.getMinimum ();
		const double interval = slider.getInterval ();
		if (minimum == 0.0 && maximum == 1.0)
		{
			if (interval != 0.0)
			{
				number = (int)(value / interval);
				number++;
			}
			else
				number = (int)(value * 100);
		}
		else
			number = (int)value;
	}

	const float rwidth = width - 2.0f;
	const float rheight = height - 2.0f;
	const float rx = x + 1.0f;
	const float ry = y + 1.0f;

	if (slider.isEnabled () && number != 0)
		g.setColour (slider.findColour (Slider::rotarySliderFillColourId));
	else
		g.setColour (Colour (0x80808080));

	g.fillRoundedRectangle (rx, ry, rwidth, rheight, 3.f);

	if (slider.isEnabled())
		g.setColour (Colour (0xff000000));
	else
		g.setColour (slider.findColour (Slider::textBoxTextColourId));

	g.drawText (String (number), (int)(rx + 4.0), (int)(ry + 1.0), (int)(rwidth - 8.0), (int)(rheight - 2.0), Justification::centredRight, false);

}