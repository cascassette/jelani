/*
  ==============================================================================

    JelaniLookAndFeel.h
    Created: 2 Mar 2014 12:47:32pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef JELANILOOKANDFEEL_H_INCLUDED
#define JELANILOOKANDFEEL_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"

class /*JUCE_API*/ JelaniLookAndFeel : public LookAndFeel_V3
{
public:
	JelaniLookAndFeel ();
	~JelaniLookAndFeel ();

	void drawRotarySlider (Graphics& g, int x, int y, int width, int height, float sliderPos,
		const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider) override;

	void drawLinearSlider (Graphics& g, int x, int y, int width, int height,
		float sliderPos, float minSliderPos, float maxSliderPos,
		const Slider::SliderStyle style, Slider& slider) override;

	void drawToggleButton (Graphics& g, ToggleButton& button,
		bool isMouseOverButton, bool isButtonDown);
};

#endif  // JELANILOOKANDFEEL_H_INCLUDED
