/*
  ==============================================================================

    JelaniRotarySliderNumber.h
    Created: 13 Mar 2014 1:51:50pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef JELANIROTARYSLIDERNUMBER_H_INCLUDED
#define JELANIROTARYSLIDERNUMBER_H_INCLUDED

#include "JelaniLookAndFeel.h"

class JelaniRotarySliderNumber : public JelaniLookAndFeel
{
	public:
		void drawRotarySlider (Graphics& g, int x, int y, int width, int height, float sliderPos,
			const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider) override;
};

#endif  // JELANIROTARYSLIDERNUMBER_H_INCLUDED
