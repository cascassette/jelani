/*
  ==============================================================================

    JelaniRotarySliderRight.cpp
    Created: 13 Mar 2014 1:51:35pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "JelaniRotarySliderRight.h"

void JelaniRotarySliderRight::drawRotarySlider (Graphics& g, int x, int y, int width, int height, float sliderPos,
	const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider)
{
	const float radius = jmin (width / 2, height / 2) - 2.0f;
	const float centreX = x + width * 0.5f;
	const float centreY = y + height * 0.5f;
	const float rx = centreX - radius;
	const float ry = centreY - radius;
	const float rw = radius * 2.0f;
	const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);
	float fillStartAngle = rotaryEndAngle;
	bool enabled = slider.isEnabled();

	if (enabled)
		g.setColour (slider.findColour (Slider::rotarySliderFillColourId));
	else
		g.setColour (Colour (0x80808080));

	Path filledArc;
	filledArc.addPieSegment (rx, ry, rw, rw, fillStartAngle, angle, .4f);
	g.fillPath (filledArc);

	if (enabled)
		g.setColour (slider.findColour (Slider::rotarySliderOutlineColourId));
	else
		g.setColour (Colour (0x80808080));

	Path outlineArc;
	outlineArc.addPieSegment (rx, ry, rw, rw, rotaryStartAngle, rotaryEndAngle, .4f);
	outlineArc.closeSubPath ();
	g.strokePath (outlineArc, PathStrokeType (slider.isEnabled () ? 1.f : 0.3f));

	if (enabled)
		g.setColour (Colour (0xff404040));
	else
		g.setColour (Colour (0x80808080));

	Path pointArc;
	pointArc.addPieSegment (rx, ry, rw, rw, angle - .15f, angle + .15f, .5f);
	g.fillPath (pointArc);
}