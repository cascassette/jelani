/*
  ==============================================================================

    JelaniRotarySliderRight.h
    Created: 13 Mar 2014 1:51:35pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef JELANIROTARYSLIDERRIGHT_H_INCLUDED
#define JELANIROTARYSLIDERRIGHT_H_INCLUDED

#include "JelaniLookAndFeel.h"

class JelaniRotarySliderRight : public JelaniLookAndFeel
{
	public:
		void drawRotarySlider (Graphics& g, int x, int y, int width, int height, float sliderPos,
			const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider) override;
};

#endif  // JELANIROTARYSLIDERRIGHT_H_INCLUDED
