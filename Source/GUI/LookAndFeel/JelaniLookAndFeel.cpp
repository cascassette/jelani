/*
  ==============================================================================

    JelaniLookAndFeel.cpp
    Created: 2 Mar 2014 12:47:32pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "JelaniLookAndFeel.h"

JelaniLookAndFeel::JelaniLookAndFeel () : LookAndFeel_V3 ()
{
	setColour (TooltipWindow::backgroundColourId, Colour (0xffcdf0ff));
	setColour (TooltipWindow::outlineColourId, Colour (0xc08ddeff));
	setColour (TooltipWindow::textColourId, Colour (0xff000000));
	setColour (Slider::textBoxTextColourId, Colour (0xff202020));
	setColour (Slider::rotarySliderFillColourId, Colour (0xff8ddeff));
	setColour (Slider::rotarySliderOutlineColourId, Colour (0xff404040));
	setColour (TextButton::buttonOnColourId, Colour (0xff8ddeff));
	setColour (TextButton::buttonColourId, Colour (0xff808080));
	setColour (TextButton::textColourOffId, Colour (0xff202020));
	setColour (TextButton::textColourOnId, Colour (0xff000000));
	setColour (PopupMenu::textColourId, Colour (0xffc0c0c0));
	setColour (PopupMenu::backgroundColourId, Colour (0xff000000));
	setColour (PopupMenu::highlightedBackgroundColourId, Colour (0xff8ddeff));
	setColour (PopupMenu::highlightedTextColourId, Colour (0xff000000));
	setColour (Label::textColourId, Colours::white);
	setColour (TextEditor::textColourId, Colours::white);
	setColour (TextEditor::backgroundColourId, Colour (0x00000000));
}

JelaniLookAndFeel::~JelaniLookAndFeel ()
{
}

void JelaniLookAndFeel::drawRotarySlider (Graphics& g, int x, int y, int width, int height, float sliderPos,
	const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider)
{
	const float radius = jmin (width / 2, height / 2) - 2.0f;
	const float centreX = x + width * 0.5f;
	const float centreY = y + height * 0.5f;
	const float rx = centreX - radius;
	const float ry = centreY - radius;
	const float rw = radius * 2.0f;
	const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);
	float fillStartAngle = rotaryStartAngle;
	
	if (slider.isEnabled ())
		g.setColour (slider.findColour (Slider::rotarySliderFillColourId));
	else
		g.setColour (Colour (0x80808080));

	Path filledArc;
	filledArc.addPieSegment (rx, ry, rw, rw, fillStartAngle, angle, .4f);
	g.fillPath (filledArc);

	if (slider.isEnabled ())
		g.setColour (slider.findColour (Slider::rotarySliderOutlineColourId));
	else
		g.setColour (Colour (0x80808080));

	Path outlineArc;
	outlineArc.addPieSegment (rx, ry, rw, rw, rotaryStartAngle, rotaryEndAngle, .4f);
	outlineArc.closeSubPath ();
	g.strokePath (outlineArc, PathStrokeType (slider.isEnabled () ? 1.f : 0.3f));

	if (slider.isEnabled ())
		g.setColour (Colour (0xff404040));
	else
		g.setColour (Colour (0x80808080));

	Path pointArc;
	pointArc.addPieSegment (rx, ry, rw, rw, angle - .15f, angle + .15f, .5f);
	g.fillPath (pointArc);
}

void JelaniLookAndFeel::drawLinearSlider (Graphics& g, int x, int y, int width, int height,
	float sliderPos, float /*minSliderPos*/, float /*maxSliderPos*/,
	const Slider::SliderStyle style, Slider& slider)
{
	g.fillAll (slider.findColour (Slider::backgroundColourId));

	const float fx = (float)x, fy = (float)y, fw = (float)width, fh = (float)height;

	g.setColour (slider.findColour (Slider::rotarySliderFillColourId).darker (.4f));
	if (style == Slider::LinearBarVertical)
		g.fillRect (fx, sliderPos, fw, 1.0f + fh - sliderPos);
	else
		g.fillRect (fx, fy, sliderPos - fx, fh);

	g.setColour (slider.findColour (Slider::rotarySliderFillColourId));
	if (style == Slider::LinearBarVertical)
		g.fillRect (fx, sliderPos - 1.0f, fw, 3.0f);
	else
		g.fillRect (sliderPos - 1.0f, fy, 3.0f, fh);
}

void JelaniLookAndFeel::drawToggleButton (Graphics& g, ToggleButton& button,
	bool /*isMouseOverButton*/, bool /*isButtonDown*/)
{
	const float rwidth = button.getWidth () - 2.0f;
	const float rheight = button.getHeight () - 2.0f;
	const float rx = 1.0f, ry = 1.0f;

	if (/*button.isEnabled () &&*/ button.getToggleState ())
		g.setColour (button.findColour (TextButton::buttonOnColourId));
	else
		g.setColour (Colour (0x80808080));

	g.fillRoundedRectangle (rx, ry, rwidth, rheight, 3.f);

	if (/*button.isEnabled () &&*/ button.getToggleState ())
	{
		//g.setColour (button.findColour (TextButton::textColourOffId).withAlpha (.4f));
		g.setColour (Colour (0x40c0c0c0));
		g.drawRoundedRectangle (rx, ry, rwidth, rheight, 3.f, 1.f);
		
		g.setColour (button.findColour (TextButton::textColourOnId));
	}
	else
	{
		g.setColour (button.findColour (TextButton::textColourOffId).withAlpha (.5f));
		g.drawRoundedRectangle (rx, ry, rwidth, rheight, 3.f, 1.f);

		g.setColour (button.findColour (TextButton::textColourOffId));
	}

	g.drawText (button.getButtonText(), (int)(rx + 1.0), (int)(ry + 1.0), (int)(rwidth - 2.0), (int)(rheight - 2.0), Justification::centred, true);
}