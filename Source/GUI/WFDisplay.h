/*
  ==============================================================================

    WFDisplay.h
    Created: 1 Mar 2014 10:22:28pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef WFDISPLAY_H_INCLUDED
#define WFDISPLAY_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../WaveTable.h"

class WFDisplay    : public Slider
{
public:
    WFDisplay(Colour fg, Colour bg);
    ~WFDisplay();

	void setWT (WaveTable* wavtab);
	void setFG (Colour fg);
	void setBG (Colour bg);

    void paint (Graphics&);
    void resized();

private:
	int width, height;
	WaveTable* wt;
	Colour clrLine, clrField;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (WFDisplay)
};


#endif  // WFDISPLAY_H_INCLUDED
