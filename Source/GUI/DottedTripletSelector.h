/*
  ==============================================================================

    DottedTripletSelector.h
    Created: 8 Mar 2014 9:51:30pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef DOTTEDTRIPLETSELECTOR_H_INCLUDED
#define DOTTEDTRIPLETSELECTOR_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../LFO.h"

// DottedTripletSelector should be sized (24, 24)
class DottedTripletSelector    : public Component
{
public:
    DottedTripletSelector();
    ~DottedTripletSelector();

    void paint (Graphics&);
    void resized();

	LFO::LFOSyncMultIds getSMID();
	void setSMID (LFO::LFOSyncMultIds);

	void setSyncEnabled (bool well);

	void mouseUp (const MouseEvent& event);

	class Listener
	{
	public:
		virtual ~Listener() {};
		virtual void syncMultIdChanged (LFO::LFOSyncMultIds newSyncMultId) = 0;
	};
	void addListener(Listener* listener);
	void removeListener(Listener* listener);

private:
	bool syncEnabled;

	LFO::LFOSyncMultIds SMID;

	Image imgTriplet, imgDotted;
	Rectangle<int> rctTriplet, rctDotted;

	ListenerList<DottedTripletSelector::Listener> listeners;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (DottedTripletSelector)
};

typedef DottedTripletSelector::Listener DottedTripletSelectorListener;

#endif  // DOTTEDTRIPLETSELECTOR_H_INCLUDED
