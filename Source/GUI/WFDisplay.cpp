/*
  ==============================================================================

    WFDisplay.cpp
    Created: 1 Mar 2014 10:22:28pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "WFDisplay.h"

WFDisplay::WFDisplay (Colour fg, Colour bg)
{
	wt = nullptr;
	clrLine = fg;
	clrField = bg;
}

WFDisplay::~WFDisplay()
{
}

void WFDisplay::setWT (WaveTable* wavtab)
{
	if (wavtab != nullptr && wt != wavtab)
	{
		wt = wavtab;
		repaint();
	}
}

void WFDisplay::setFG (Colour fg)
{
	clrLine = fg;
	repaint();
}

void WFDisplay::setBG (Colour bg)
{
	clrField = bg;
	repaint();
}

void WFDisplay::paint (Graphics& g)
{
	g.fillAll (clrField);
	if (wt != nullptr)
	{
		g.setColour (clrLine);
		float mid = height / 2.f;
		float y1, y2;
		y1 = 0;
		for (int i = 0; i < width; i++)
		{
			y2 = wt->Lookup ((double)i / width, WaveTable::None);
			g.drawLine ((float)i - 1, mid - mid * y1 + 1, (float)i, mid - mid * y2 + 1, 1.5f);
			y1 = y2;
		}
	}
}

void WFDisplay::resized()
{
	width = this->getWidth();
	height = this->getHeight() - 2; // leave a little room at the edge
}
