/*
  ==============================================================================

	const.h
	Created: 23 Jan 2014 2:35:24pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef CONST_H_INCLUDED
#define CONST_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

// Defaults to compile with
// All but kWaveForms can be changed without further ripples
enum { kWaveSize = 4096, maxVoices = 64, kOps = 6, kWaveForms = 15 };

const int defaultNumVoices = 8;

// 
const double juceSynthTailLength = 5.;

// The MinInfinitydB values for various operator..
const float opLevelMinInfdB = -30.f;
const float opModulMinInfdB = -60.f;
const float opPostAMinInfdB = -30.f;
// and global level controls
const float outLevelMinInfdB = -60.f;

// Envelope sustain dB mapping
const float sustainRangedB = 70.f;
const float opSustnMinInfdB = -70.f;

// Maximum modulation in dB for the mod matrix
const float maxModdB = 15.f;

// Maximum operator ratio
const float maxRatio = 16.f;

// Lots of operator parameter defaults
// NB these are _external_ values [0..1)
const float defaultRatio = 1./16.;
const float defaultSTOff = 0.5f;
const float defaultHzOff = 0.5f;
const float defaultLevel = 1.0f;
const float defaultVSens = 1.0f;
const float defaultKeySc = 0.5f;
const float defaultModul = 0.0f;
const float defaultPostA = 0.0f;
const float defaultAttck = 0.01f;
const float defaultDecay = 0.02f;
const float defaultSustn = 0.75f;
const float defaultRelea = 0.04f;
const float defaultPEAttck = 0.0f;
const float defaultPEDecay = 0.1f;
const float defaultLFOSpeed = 0.1f;
const float defaultSynthOutLevel = 0.9f;

// Skew factors for some GUI elements
const float defaultVSensSkew = 0.5f;
const float defaultEnvTimeSkew = 0.5f;
const float defaultLFOSpeedSkew = 0.25f;

// Time in ms to refresh
const int refreshGUI = 250;

// Envelope max times
const float maxAtt = 30000.f;
const float maxDec = 30000.f;
const float maxRel = 30000.f;
const float maxAttPitch = 1000.f;
const float maxDecPitch = 1000.f;

const float maxGlideTime = 1000.f;

const float maxLFOSpeed = 20.f;

const Colour clrWhite = Colour (0xffffffff);
const Colour clrTransparentWhite = Colour (0x80ffffff);
const Colour clrBright = Colour (0xffd0d0d0);
const Colour clrDim = Colour (0xff909090);
const Colour clrDark = Colour (0xff202020);
const Colour clrJelaniBlue = Colour (0xff8ddeff);
const Colour clrTransparentGrey = Colour (0x80808080);

#endif	// CONST_H_INCLUDED
