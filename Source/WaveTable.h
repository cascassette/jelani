/*
  ==============================================================================

	WaveTable.h
	Created: 21 Jan 2014 10:18:51pm
	Author:  Cas Cassette

	Defines the WaveTable class, which can hold tables of audio and do
	interpolated lookups.

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef WAVETABLE_H_INCLUDED
#define WAVETABLE_H_INCLUDED
#include "../JuceLibraryCode/JuceHeader.h"

#include <vector>

class WaveTable
{
public:
//	String name;
	// Default constructor (useless)
	WaveTable();
	// Initialize correctly right away
	WaveTable (float* wavtab, int length);

	// You gotta load in a wavtab first so yeah
	// use this to check if you did
	bool isReady() { return ready; }

	// Choose lookup method / interpolation
	void setDefaultLookupMethod (int lookupMethod);
	// Do wavetable lookup
	float Lookup (double phase);
	float Lookup (double phase, int lookupMethod);

	enum LookupMethods
	{
		None = 0,
		Linear,
//		Cubic,

		totalNumLookupMethods
	};
private:
	bool   ready;
	int    lookupMethod;
	int    len;
	std::vector<float> wt;
};



#endif	// WAVETABLE_H_INCLUDED
