/*
  ==============================================================================

	Operator.cpp
	Created: 22 Jan 2014 12:26:17am
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "WaveTable.h"
#include "Operator.h"
#include "const.h"

Operator::Operator ()
{
	phase = 0;
	activ = true;
	level = defaultLevel;
	ratio = defaultRatio;
	currentPhase = phase;
	envgen = ADSR();
}

void Operator::setActiv (bool a)
{
	activ = a;
}

void Operator::setWaveTable (WaveTable* wavtab)
{
	allwt = wavtab;
}

void Operator::setSampleRate (double sampleRate)
{
	envgen.setSampleRate (sampleRate);
}

void Operator::setWForm (int w)
{
	if (w >= 0 && w < kWaveForms)
	{
		wform = w;
		wt = allwt+w;
	}
}

void Operator::setRatio (float r)
{
	ratio = r;
}

void Operator::setSTOff (float o)
{
	stoff = o;
}

void Operator::setHzOff (float h)
{
	hzoff = h;
}

void Operator::setLFOPi (float p)
{
	lfopi = p;
}

void Operator::setPEAmt (float a)
{
	peamt = a;
}

void Operator::setPhase (float p)
{
	phase = p;
}

void Operator::setLevel (float l)
{
	level = Decibels::decibelsToGain (l, opLevelMinInfdB);
}

void Operator::setVSens (float v)
{
	vsens = v;
}

void Operator::setKeySc (float k)
{
	keysc = k;
}

void Operator::setLFOAm (float a)
{
	lfoam = a;
}

void Operator::setPostA (float a)
{
	posta = Decibels::decibelsToGain (a, opPostAMinInfdB);
}

void Operator::setKTime (float k)
{
	ktime = k;
}

void Operator::setAttck (float att)
{
	envgen.setAttack (att);
}

void Operator::setDecay (float dec)
{
	envgen.setDecay (dec);
}

void Operator::setSustn (float sus)
{
	envgen.setSustain (sus);
}

void Operator::setRelea (float rel)
{
	envgen.setRelease (rel);
}

void Operator::noteOn (double sampleRate, int midiNoteNumber, float velocity)
{
	currentPhase = phase;
	float gfactor = (velocity * vsens + (1.0f - vsens)) * getVolumeScaling (midiNoteNumber);
	amp = level * gfactor;
	tailOff = 0.0;

	double cyclesPerSecond = getMidiNoteInHertz ((float)midiNoteNumber) * ratio;
	cyclesPerSecond *= getMidiIntervalInRatio (stoff);
	cyclesPerSecond += hzoff;
	phaseDelta = cyclesPerSecond / sampleRate;

	float tfactor = getTimeScaling (midiNoteNumber);
	envgen.noteOn (tfactor);
}

void Operator::noteOff()
{
	envgen.noteOff();
}

float Operator::getVolumeScaling (int midiNoteNumber)
{
	return pow (getMidiIntervalInRatio (midiNoteNumber - 48), -keysc);
}

float Operator::getTimeScaling (int midiNoteNumber)
{
	return pow (getMidiIntervalInRatio (midiNoteNumber - 48), ktime);
}

double Operator::getMidiNoteInHertz (float noteNumber, const double frequencyOfA)
{
	return frequencyOfA * pow (2.0, (noteNumber - 69) / 12.0);
}

double Operator::getMidiIntervalInRatio (double noteInterval)
{
	return pow (2.0, noteInterval / 12.0);
}

double Operator::nextSample (double deltaPhase, double lfoVal, double peVal, double glVal)
{
	currentPhase += phaseDelta * getMidiIntervalInRatio (lfopi * lfoVal + peamt * peVal + glVal);
	if (currentPhase > 1.0) currentPhase -= 1.0;
	else if (currentPhase < 0.0) currentPhase += 1.0;
	
	return currentSample(deltaPhase, lfoVal);
}

double Operator::currentSample (double deltaPhase, double lfoVal)
{
	float envamp = Decibels::decibelsToGain (envgen.process() * sustainRangedB - sustainRangedB, opSustnMinInfdB);
	return (float) wt->Lookup (currentPhase + deltaPhase) * amp * envamp * Decibels::decibelsToGain (lfoam * lfoVal);
}

const char* Operator::getOperatorName (int o)
{
	char* res = new char[2];
	res[0] = 'A' + o;
	res[1] = 0;
	return res;
	//return String::charToString ('A' + o).getCharPointer();
}

const char* Operator::getParameterName (int p)
{
	switch (p)
	{
	case Activ: return "ACTIV";
	case WForm: return "WFORM";
	case Ratio: return "RATIO";
	case STOff: return "STOFF";
	case HzOff: return "HZOFF";
	case LFOPi: return "LFOPI";
	case PEAmt: return "PEAMT";
	case Phase: return "PHASE";
	case Level: return "LEVEL";
	case VSens: return "VSENS";
	case KeySc: return "KEYSC";
	case LFOAm: return "LFOAM";
	case KTime: return "KTIME";
	case Attck: return "ATTCK";
	case Decay: return "DECAY";
	case Sustn: return "SUSTN";
	case Relea: return "RELEA";
	case PostA: return "POSTA";
	}
	return "";
}

const char* Operator::getFriendlyParameterName (int p)
{
	switch (p)
	{
	case Activ: return "On";
	case WForm: return "Waveform";
	case Ratio: return "Ratio";
	case STOff: return "Semitones";
	case HzOff: return "Pitch offset";
	case LFOPi: return "LFO>Pitch";
	case PEAmt: return "Env>Pitch";
	case Phase: return "Phase offset";
	case Level: return "PreAmp";
	case VSens: return "Velocity sensitivity";
	case KeySc: return "Key>Amp";
	case LFOAm: return "LFO>Amp";
	case KTime: return "Key>EnvTime";
	case Attck: return "Env Attack";
	case Decay: return "Env Decay";
	case Sustn: return "Env Sustain";
	case Relea: return "Env Release";
	case PostA: return "Post Gain";
	}
	return "";
}

const char* Operator::getFriendlyParameterUnit (int p)
{
	switch (p)
	{
	case Activ: return "";
	case WForm: return "";
	case Ratio: return "";
	case STOff: return "(semitones)";
	case HzOff: return "(Hz)";
	case LFOPi: return "(semitones)";
	case PEAmt: return "(semitones)";
	case Phase: return "";
	case Level: return "(dB)";
	case VSens: return "%";
	case KeySc: return "";
	case LFOAm: return "(dB)";
	case KTime: return "";
	case Attck: return "(ms)";
	case Decay: return "(ms)";
	case Sustn: return "";
	case Relea: return "(ms)";
	case PostA: return "(dB)";
	}
	return "";
}

const char* Operator::getFriendlyWFName (int w)
{
	switch (w)
	{
	case Sine:		 return "Sine";
	case Sine12:	 return "Sine 1+2";
	case Sine13:	 return "Sine 1+3";
	case Sine123Saw: return "Sine 1+2+3 Saw";
	case Sine135Squ: return "Sine 1+3+5 Square";
	case Sine14:	 return "Sine 1+4";
	case Sine15:	 return "Sine 1+5";
	case Sine16:	 return "Sine 1+6";
	case Sine17:	 return "Sine 1+7";
	case Sine18:	 return "Sine 1+8";
	case Triangle:	 return "Triangle";
	case Square:	 return "Square";
	case Saw:		 return "Sawtooth";
	case Parabol:	 return "Parabol";
	case SoftSquare: return "Soft Square";
	}
	return "";
}
