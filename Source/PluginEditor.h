/*
  ==============================================================================
  
	PluginEditor.cpp
	Created: 22 Jan 2014 12:26:17am
	Author:  Cas Cassette
	
	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef __JUCE_HEADER_4696C085ABCCF8DA__
#define __JUCE_HEADER_4696C085ABCCF8DA__

#include "JuceHeader.h"
#include "PluginProcessor.h"
#include "GUI/OperatorGUI.h"
#include "GUI/DottedTripletSelector.h"
#include "GUI/LookAndFeel/JelaniLookAndFeel.h"
#include "GUI/LookAndFeel/JelaniRotarySliderNumber.h"

class JelaniFMSynthAudioProcessorEditor  : public AudioProcessorEditor,
									  public Timer,
									  public SliderListener,
									  public OperatorGUIListener,
									  public ButtonListener,
									  public ComboBoxListener,
									  public DottedTripletSelectorListener
{
public:
	// Constructor - keep a pointer to JelaniFMSynthAudioProcessor in mind
	JelaniFMSynthAudioProcessorEditor (JelaniFMSynthAudioProcessor* ownerFilter);
	~JelaniFMSynthAudioProcessorEditor();

	// Regularly check back with JelaniFMSynthAudioProcessor
	void timerCallback();
	void updateGUI();

	// Mainly used to get parameter values
	JelaniFMSynthAudioProcessor* getProcessor() const
	{
		return static_cast <JelaniFMSynthAudioProcessor*> (getAudioProcessor());
	}
	
	// Callbacks for the direct-child controls (global params, load and save buttons)
	void sliderValueChanged (Slider* sliderThatWasMoved);
	void buttonClicked (Button* buttonThatWasClicked);
	void buttonStateChanged (Button* button);
	void comboBoxChanged (ComboBox* cbx);

	// Callbacks for OperatorGUIListener
	void operatorParameterChanged (int oIdx, int pIdx, float newValue);
	void modParameterChanged (int fromIdx, int toIdx, float newValue);
	void copyEnvelopeBuffer (int fromIdx, float *ADSR);
	void pasteEnvelopeBuffer (int toIdx);

	// Callbacks for DottedTripletSelectorListener
	void syncMultIdChanged (LFO::LFOSyncMultIds newSyncMultId);

	// Paint it black
	void paint (Graphics& g);

	// Makes everything fit
	void resized();

private:
	void updateSyncState();
	void updateLFOSpeedValue();

	float envBuf[ADSR::totalNumEnvPhases];

	Rectangle<float> rctLFO, rctLFOSYNC, rctLFONOSYNC, rctPEN, rctPGM, rctGLIDE, rctVCS;

	bool glideEnabled;

	OwnedArray<OperatorGUI> arOG;
	TooltipWindow ttw;

	ScopedPointer<ImageComponent> imgComponentLogo;
	ScopedPointer<ToggleButton> tglApplyToAll;
	ScopedPointer<Label> lblWForm;
	ScopedPointer<Label> lblRatio;
	ScopedPointer<Label> lblSTOff;
	ScopedPointer<Label> lblHzOff;
	ScopedPointer<Label> lblLFOPi;
	ScopedPointer<Label> lblPEAmt;
	ScopedPointer<Label> lblPhase;
	ScopedPointer<Label> lblAmp;
	ScopedPointer<Label> lblVS;
	ScopedPointer<Label> lblKey;
	ScopedPointer<Label> lblLFO;
	ScopedPointer<Label> lblKTi;
	ScopedPointer<Label> lblAtt;
	ScopedPointer<Label> lblDec;
	ScopedPointer<Label> lblSus;
	ScopedPointer<Label> lblRel;
	ScopedPointer<Label> lblMod;
	ScopedPointer<Label> lblOut;
	ScopedPointer<Label> lblPgm;
	ScopedPointer<Label> lblVcs;
	ScopedPointer<Label> lblPEn;
	ScopedPointer<Label> lblLSp;
	ScopedPointer<Label> lblLastAction;
	ScopedPointer<Slider> sldOutLevel;
	ScopedPointer<Slider> sldLFOSpeed;
	ScopedPointer<Slider> sldPEAttck;
	ScopedPointer<Slider> sldPEDecay;
	ScopedPointer<ImageButton> btnLoad;
	ScopedPointer<ImageButton> btnSave;
	ScopedPointer<ImageButton> btnReset;
	ScopedPointer<ComboBox> cbxPrograms;
	ScopedPointer<Slider> sldVoices;
	ScopedPointer<ToggleButton> tglSyncLFO;
	ScopedPointer<DottedTripletSelector> DTS;
	ScopedPointer<Label> lblGlide;
	ScopedPointer<Slider> sldGlide;

	ScopedPointer<JelaniLookAndFeel> laf;
	ScopedPointer<JelaniRotarySliderNumber> laf_number;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (JelaniFMSynthAudioProcessorEditor)
};

#endif	 // __JUCE_HEADER_4696C085ABCCF8DA__
