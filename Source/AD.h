/*
  ==============================================================================

    AD.h
    Created: 28 Feb 2014 2:59:59pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef AD_H_INCLUDED
#define AD_H_INCLUDED

class AD
{
public:
	AD ();

	// Must be called before setStuff, process/getOutput functions
	void setSampleRate (double sampleRate);

	// Reacts to midi input from FMVoice
	void noteOn();
	void noteOff();

	// Advances sample and returns new gain value
	float process();
	// Returns gain value without advancing through time
	float getOutput() { return output; }

	// Getters & setters
	float getAttack()  { return att; }
	float getDecay()   { return dec; }
	int   getState()   { return state; }
	
	void setAttack (float newAtt);
	void setDecay	(float newDec);

	// Kill env state
	void reset ();

	enum Phases
	{
		Idle = 0,
		Attack,
		Decay,

		totalNumEnvPhases
	};

protected:
	int state;
	float output;
	float att, dec;
	double deltaGAtt, deltaGDec;

	// samples / msec
	double spms;
};

#endif  // AD_H_INCLUDED
