/*
  ==============================================================================

	WaveTable.cpp
	Created: 21 Jan 2014 10:18:51pm
	Author:  Cas Cassette

	Defines the WaveTable class, which can hold tables of audio and do
	interpolated lookups.
	
	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "WaveTable.h"
#include <math.h>

int defaultLookupMethod = WaveTable::Linear;

WaveTable::WaveTable()
{
	ready = false;
}

WaveTable::WaveTable (float* wavtab, int length)
{
	len = length;
	wt = std::vector<float> (wavtab, wavtab+len);
	lookupMethod = defaultLookupMethod;
	ready = true;
}

void WaveTable::setDefaultLookupMethod (int lm)
{
	if (lm >= 0 && lm < totalNumLookupMethods )
		lookupMethod = lm;
}

float WaveTable::Lookup (double phase)
{
	return Lookup(phase, defaultLookupMethod);
}

float WaveTable::Lookup (double phase, int lookupMethod)
{
	if (ready)
	{
		switch (lookupMethod)
		{
		case None:
			phase -= (double)(floor(phase));			// modulo 1 - phase goes [0..1)
			if (phase == 1.0) phase = 0.0;				// to fix when phase would be exactly 1.0000000
			return wt[(long)(phase*len)];

		case Linear:
			phase -= (double)(floor(phase));			// modulo 1 - phase goes [0..1)
			if (phase == 1.0) phase = 0.0;				// to fix when phase would be exactly 1.0000000
			double phaseInSamples = phase * len;		// where in table is this
			int x0 = (int)(floor(phaseInSamples));		// index left of the mu value
			double mu = phaseInSamples - x0;			// distance from x0 in units
			int x1 = (x0 + 1) % len;					// index right of the mu
			float y0 = wt[x0];							// value on the left
			float y1 = wt[x1];							// value on the right

			return (float)((1 - mu) * y0 + (mu) * y1);
		}
	}
	return 0.0;
}
