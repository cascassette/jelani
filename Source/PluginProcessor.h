/*
  ==============================================================================

	PluginProcessor.h
	Created: 7 Jan 2014 10:14:23am
	Author:  Cas Cassette

	Plugin Processor code.

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Operator.h"
#include "WaveTable.h"
#include "const.h"
#include "Parameter/Parameter.h"
#include "LFO.h"

class JelaniFMSynthAudioProcessor  : public AudioProcessor
{
public:
	// Constructor - intializes wavetables, voices, parameters
	JelaniFMSynthAudioProcessor();
	// Destructor
	~JelaniFMSynthAudioProcessor();

	// Gives sample rate info to LFO, Synthesizer, resets midiKBstate
	void prepareToPlay (double sampleRate, int samplesPerBlock);
	// Resets midiKBstate
	void releaseResources();
	// Calcs LFO, processes midi events, calls on synth to generate block of samples
	void processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages);
	// Resets midiKBstate, resets synth / all voices
	void reset() override;

	// Makes a JelaniFMSynthAudioProcessorEditor
	AudioProcessorEditor* createEditor();
	bool hasEditor() const override						{ return true; }

	// Guess what
	const String getName() const;

	// Parameter getters & setters for host
	int getNumParameters();
	float getParameter (int index);
	void setParameter (int index, float newValue);
	// Parameter getters & setters for processor, synth, voice, operator
	// getParameterInternal is used mostly for display in statusbar (GUI)
	// parameters that use setParameterInternal are the ones that have a textbox in the GUI
	float getParameterInternal (int index);
	void setParameterInternal (int index, float newValue);
	void setParameterInternalNotifyingHost (int index, float newValue); // helper function like juces setParameterNotifyingHost

	// Parameter name for host
	const String getParameterName (int index);
	// Parameter value in string form (could include unit) for host
	const String getParameterText (int index);

	// Channel info for host
	const String getInputChannelName (int channelIndex) const;
	const String getOutputChannelName (int channelIndex) const;
	bool isInputChannelStereoPair (int index) const;
	bool isOutputChannelStereoPair (int index) const;

	// Miscellaneous juce plugin characteristics
	bool acceptsMidi() const;
	bool producesMidi() const;
	bool silenceInProducesSilenceOut() const;
	double getTailLengthSeconds() const;

	// Preset patches ('programs')
	int getNumPrograms();
	int getCurrentProgram();
	void setCurrentProgram (int index);
	const String getProgramName (int index);
	void changeProgramName (int /*index*/, const String& /*newName*/) override	{}

	// State saving and loading routines used by host
	void getStateInformation (MemoryBlock& destData);
	void setStateInformation (const void* data, int sizeInBytes);

	// Helper functions to enable custom state saving/loading functionality
	// ie these serialize the complete state of the synth
	// using Operator::getParameterName
	XmlElement* getState ();
	void setState (XmlElement* xml);

	// Keeps tabs on the midi note presses
	MidiKeyboardState keyboardState;

	// Global parameters
	enum Parameters
	{
		OutLevel = 0,
		LFOSpeed,
		PEAttck,
		PEDecay,
		NumVoices,
		LFOSync,
		LFOSyncId,
		LFOSyncMultId,
		GlideTime,

		numRegularParams
	};

	// Getter and setter for UIUpdateFlag - way for the GUI to know if some
	// setting has changed that needs to be reflected by updating/drawing
	bool NeedsUIUpdate() { return UIUpdateFlag; }
	void ClearUIUpdateFlag() { UIUpdateFlag = false; }

	// For operators to 'reach out and touch' via FMVoice
	WaveTable* getWT (int w);

	// Parameter 'area' delimiters - see below
	static const int startOfOpParams = numRegularParams;
	static const int startOfModParams = numRegularParams + kOps * Operator::totalNumParamsPerOperator;
	static const int endOfAllParams = startOfModParams + kOps * kOps;

	File getSettingsDirectory() { return settingsDirectory; }

	int getNumVoices() { return synth.getNumVoices(); }
	void setNumVoices (int nv);

	bool getGlideEnabled() { return glide; }

	// Get LFO speed - for GUI
	double getLFOSpeed();
	void paramReset ();

private:
	WaveTable WaveTables[kWaveForms];
	void addNewVoiceToSynth();

	File settingsDirectory;
	int programCount;
	int programLoaded;
	StringArray programnames;
	//StringArray programs;

	// Parameters are reported to host in following order (and magnitude) linearly:
	/*float Params[numRegularParams];
	float opParams[kOps][Operator::totalNumParamsPerOperator];
	float modParams[kOps][kOps];*/

	// new style
	// TODO; have one proper array of parameters so specifics are handled where absolutely needed
	//       this needs a translation function paramno -> regular,
	//                                                    opparam + op, or
	//                                                    modparamfrom + to
	Parameter* Params[numRegularParams];
	Parameter* opParams[kOps][Operator::totalNumParamsPerOperator];
	Parameter* modParams[kOps][kOps];

	// LFO synced or not
	bool lfosync;

	// Glide enabled or not; equals (Params[NumVoices] == 1)
	bool glide;

	// Global LFO routable to operator pitch, amp
	LFO globalLFO;
	// Buffer to keep next block of samples for LFO to keep thread/voice/op safety
	double* LFOBuf;
	bool LFOBufInitialised;
	// BPM
	double bpm;

	// See above (NeedsUIUpdate())
	bool UIUpdateFlag;

	// The boss of sounds and voices and such
	Synthesiser synth;

	//==============================================================================
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (JelaniFMSynthAudioProcessor)
};

#endif	// PLUGINPROCESSOR_H_INCLUDED
