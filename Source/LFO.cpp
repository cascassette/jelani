/*
  ==============================================================================

	LFO.cpp
	Created: 14 Feb 2014 11:13:23pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "LFO.h"
#include "../JuceLibraryCode/JuceHeader.h"

const double maxPhase = 2.0 * double_Pi;

LFO::LFO()
{
	phase = phaseDelta = 0.;
	LFOSyncFreqMultValues[Dotted] = 2. / 3.;
	LFOSyncFreqMultValues[Normal] = 1.;
	LFOSyncFreqMultValues[Triplet] = 3. / 2.;
}

void LFO::setSpeedInHz (double speed, double sampleRate)
{
	if (sampleRate == 0) return;
	phaseDelta = maxPhase * speed / sampleRate;
}

void LFO::setSpeedInBeats (double bpm, int syncId, int syncMultId, double sampleRate)
{
	if (bpm > 0 && sampleRate != 0 &&
		syncId >= 0 && syncId < numLFOSyncIds &&
		syncMultId >= 0 && syncMultId < numLFOSyncMultIds)
	{
		int lfosyncstartpower = 4; // 1/32nd bars = 1/8th in beats
		double spd = bpm / 60.;
		spd *= pow (2., (double)(syncId - lfosyncstartpower));
		spd *= LFOSyncFreqMultValues[syncMultId];

		setSpeedInHz (spd, sampleRate);
	}
}

double LFO::getSpeedInHz(double sampleRate)
{
	if (phaseDelta == 0 || sampleRate == 0) return 0.;
	return phaseDelta * sampleRate / maxPhase;
}

double LFO::getNextValue()
{
	if (phaseDelta == 0) return 0.;
	phase += phaseDelta;
	if (phase >= maxPhase) phase -= maxPhase;
	return getCurrentValue ();
}

double LFO::getCurrentValue()
{
	return sin (phase);
}

const String LFO::getSyncSpeedName (int syncId, int syncMultId)
{
	String res;
	switch (syncId)
	{
	case One32nd:	res = "1/32"; break;
	case One16th:	res = "1/16"; break;
	case One8th:	res = "1/8"; break;
	case One4th:	res = "1/4"; break;
	case One2nd:	res = "1/2"; break;
	case One:		res = "1/1"; break;
	case Two:		res = "2/1"; break;
	case Four:		res = "4/1"; break;
	default:		break;
	}
	switch (syncMultId)
	{
	case Dotted:	res += " D"; break;
	case Triplet:	res += " T"; break;
	default:		break;
	}
	return res;
}