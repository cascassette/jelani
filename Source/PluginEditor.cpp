/*
  ==============================================================================
  
	PluginEditor.cpp
	Created: 22 Jan 2014 12:26:17am
	Author:  Cas Cassette
	
	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#include "PluginEditor.h"

//==============================================================================
JelaniFMSynthAudioProcessorEditor::JelaniFMSynthAudioProcessorEditor (JelaniFMSynthAudioProcessor* ownerFilter)
	: AudioProcessorEditor(ownerFilter)
{
	// init envelope copy/paste buffer
	envBuf[ADSR::Attack]  = defaultAttck;
	envBuf[ADSR::Decay]   = defaultDecay;
	envBuf[ADSR::Sustain] = defaultSustn;
	envBuf[ADSR::Release] = defaultRelea;

	// set custom LookAndFeel
	laf = new JelaniLookAndFeel();
	LookAndFeel::setDefaultLookAndFeel (laf);
	laf_number = new JelaniRotarySliderNumber();

	Image imgLogo = ImageCache::getFromMemory (BinaryData::jelanilogoblue_png, BinaryData::jelanilogoblue_pngSize);
	Image imgLoad = ImageCache::getFromMemory (BinaryData::loadblue_png, BinaryData::loadblue_pngSize);
	Image imgSave = ImageCache::getFromMemory (BinaryData::saveblue_png, BinaryData::saveblue_pngSize);
	Image imgReset = ImageCache::getFromMemory (BinaryData::reset_png, BinaryData::reset_pngSize);

	addAndMakeVisible (imgComponentLogo = new ImageComponent("new image"));
	imgComponentLogo->setImage (imgLogo);

	addAndMakeVisible (tglApplyToAll = new ToggleButton (" "));
	tglApplyToAll->setTooltip ("Apply to all");
	tglApplyToAll->addListener (this);

	addAndMakeVisible (lblWForm = new Label ("new label", "WAVE"));
	lblWForm->setFont (Font (15.00f, Font::plain));
	lblWForm->setJustificationType (Justification::centredLeft);
	lblWForm->setEditable (false, false, false);

	addAndMakeVisible (lblRatio = new Label ("new label", "RATIO"));
	lblRatio->setFont (Font (15.00f, Font::plain));
	lblRatio->setJustificationType (Justification::centredLeft);
	lblRatio->setEditable (false, false, false);

	addAndMakeVisible (lblSTOff = new Label ("new label", "SEMI"));
	lblSTOff->setFont (Font (15.00f, Font::plain));
	lblSTOff->setJustificationType (Justification::centredLeft);
	lblSTOff->setEditable (false, false, false);

	addAndMakeVisible (lblHzOff = new Label ("new label", "HERTZ"));
	lblHzOff->setFont (Font (15.00f, Font::plain));
	lblHzOff->setJustificationType (Justification::centredLeft);
	lblHzOff->setEditable (false, false, false);

	addAndMakeVisible (lblLFOPi = new Label ("new label", "LFO"));
	lblLFOPi->setFont (Font (15.00f, Font::plain));
	lblLFOPi->setJustificationType (Justification::centredLeft);
	lblLFOPi->setEditable (false, false, false);

	addAndMakeVisible (lblPEAmt = new Label ("new label", "ENV"));
	lblPEAmt->setFont (Font (15.00f, Font::plain));
	lblPEAmt->setJustificationType (Justification::centredLeft);
	lblPEAmt->setEditable (false, false, false);

	addAndMakeVisible (lblPhase = new Label ("new label", "PHS"));
	lblPhase->setFont (Font (15.00f, Font::plain));
	lblPhase->setJustificationType (Justification::centredLeft);
	lblPhase->setEditable (false, false, false);

	addAndMakeVisible (lblAmp = new Label ("new label", "LVL"));
	lblAmp->setFont (Font (15.00f, Font::plain));
	lblAmp->setJustificationType (Justification::centredLeft);
	lblAmp->setEditable (false, false, false);

	addAndMakeVisible (lblMod = new Label ("new label", "MOD"));
	lblMod->setFont (Font (15.00f, Font::plain));
	lblMod->setJustificationType (Justification::centredLeft);
	lblMod->setEditable (false, false, false);

	addAndMakeVisible (lblVS = new Label ("new label", "VEL"));
	lblVS->setFont (Font (15.00f, Font::plain));
	lblVS->setJustificationType (Justification::centredLeft);
	lblVS->setEditable (false, false, false);

	addAndMakeVisible (lblKey = new Label ("new label", "KEY"));
	lblKey->setFont (Font (15.00f, Font::plain));
	lblKey->setJustificationType (Justification::centredLeft);
	lblKey->setEditable (false, false, false);

	addAndMakeVisible (lblLFO = new Label ("new label", "LFO"));
	lblLFO->setFont (Font (15.00f, Font::plain));
	lblLFO->setJustificationType (Justification::centredLeft);
	lblLFO->setEditable (false, false, false);

	addAndMakeVisible (lblKTi = new Label ("new label", "KEY"));
	lblKTi->setFont (Font (15.00f, Font::plain));
	lblKTi->setJustificationType (Justification::centredLeft);
	lblKTi->setEditable (false, false, false);

	addAndMakeVisible (lblAtt = new Label ("new label", "A"));
	lblAtt->setFont (Font (15.00f, Font::plain));
	lblAtt->setJustificationType (Justification::centredLeft);
	lblAtt->setEditable (false, false, false);

	addAndMakeVisible (lblDec = new Label ("new label", "D"));
	lblDec->setFont (Font (15.00f, Font::plain));
	lblDec->setJustificationType (Justification::centredLeft);
	lblDec->setEditable (false, false, false);

	addAndMakeVisible (lblSus = new Label ("new label", "S"));
	lblSus->setFont (Font (15.00f, Font::plain));
	lblSus->setJustificationType (Justification::centredLeft);
	lblSus->setEditable (false, false, false);

	addAndMakeVisible (lblRel = new Label ("new label", "R"));
	lblRel->setFont (Font (15.00f, Font::plain));
	lblRel->setJustificationType (Justification::centredLeft);
	lblRel->setEditable (false, false, false);

	addAndMakeVisible (lblOut = new Label ("new label", "OUT"));
	lblOut->setFont (Font (15.00f, Font::plain));
	lblOut->setJustificationType (Justification::centredLeft);
	lblOut->setEditable (false, false, false);

	addAndMakeVisible (lblLastAction = new Label ("new label", "Init"));
	lblLastAction->setFont (Font (15.00f, Font::plain));
	lblLastAction->setJustificationType (Justification::centredLeft);
	lblLastAction->setEditable (false, false, false);
	lblLastAction->setColour (Label::textColourId, Colours::lightgrey);
	lblLastAction->setColour (TextEditor::textColourId, Colours::black);
	lblLastAction->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

	addAndMakeVisible (sldLFOSpeed = new Slider ("new slider"));
	sldLFOSpeed->setRange (0, 1, 0);
	sldLFOSpeed->setTooltip ("LFO Speed");
	sldLFOSpeed->setSliderStyle (Slider::RotaryVerticalDrag);
	sldLFOSpeed->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldLFOSpeed->setSkewFactor (defaultLFOSpeedSkew);
	sldLFOSpeed->addListener (this);

	addAndMakeVisible (sldOutLevel = new Slider ("new slider"));
	sldOutLevel->setRange (outLevelMinInfdB, 0, 0.5);
	sldOutLevel->setTooltip ("Out");
	sldOutLevel->setSliderStyle (Slider::LinearBarVertical);
	sldOutLevel->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	//sldOutLevel->setPopupDisplayEnabled (true, 0); // make sure this doesn't bug vst hosts
	sldOutLevel->addListener (this);

	addAndMakeVisible (sldPEAttck = new Slider ("new slider"));
	sldPEAttck->setRange (0, 1, 0);
	sldPEAttck->setTooltip ("P.E. Attack");
	sldPEAttck->setSliderStyle (Slider::RotaryVerticalDrag);
	sldPEAttck->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldPEAttck->setSkewFactor (defaultEnvTimeSkew);
	sldPEAttck->addListener (this);

	addAndMakeVisible (sldPEDecay = new Slider ("new slider"));
	sldPEDecay->setRange (0, 1, 0);
	sldPEDecay->setTooltip ("P.E. Attack");
	sldPEDecay->setSliderStyle (Slider::RotaryVerticalDrag);
	sldPEDecay->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldPEDecay->setSkewFactor (defaultEnvTimeSkew);
	sldPEDecay->addListener (this);

	addAndMakeVisible (btnLoad = new ImageButton (">LOAD"));
	btnLoad->setTooltip ("Load patch");
	btnLoad->setImages (false, true, true,
	                    imgLoad, .8f, Colours::transparentBlack,
						imgLoad, 1.f, Colour (0x30ffffff),
						imgLoad, 1.f, Colour (0x60ffffff),
						0.5f);
	btnLoad->addListener (this);

	addAndMakeVisible (btnSave = new ImageButton ("SAVE>"));
	btnSave->setTooltip ("Save patch");
	btnSave->setImages (false, true, true,
	                    imgSave, .8f, Colours::transparentBlack,
						imgSave, 1.f, Colour (0x30ffffff),
						imgSave, 1.f, Colour (0x60ffffff),
						0.5f);
	btnSave->addListener (this);

	addAndMakeVisible (btnReset = new ImageButton (">RESET<"));
	btnReset->setTooltip ("Init patch");
	btnReset->setImages (false, true, true,
						 imgReset, .8f, Colours::transparentBlack,
						 imgReset, 1.f, Colour (0x30ffffff),
						 imgReset, 1.f, Colour (0x60ffffff),
						 0.5f);
	btnReset->addListener (this);

	addAndMakeVisible (lblPgm = new Label ("new label", "PROGRAM:"));
	lblPgm->setFont (Font (15.00f, Font::plain));
	lblPgm->setJustificationType (Justification::centredLeft);
	lblPgm->setEditable (false, false, false);

	addAndMakeVisible (lblVcs = new Label ("new label", "VOICES:"));
	lblVcs->setFont (Font (15.00f, Font::plain));
	lblVcs->setJustificationType (Justification::centredLeft);
	lblVcs->setEditable (false, false, false);

	addAndMakeVisible (lblPEn = new Label ("new label", "PITCH ENV"));
	lblPEn->setJustificationType (Justification::centredLeft);
	lblPEn->setEditable (false, false, false);

	addAndMakeVisible (lblLSp = new Label ("new label", "LFO SPD"));
	lblLSp->setJustificationType (Justification::centredRight);
	lblLSp->setEditable (false, false, false);

	addAndMakeVisible (cbxPrograms = new ComboBox ("new combobox"));
	cbxPrograms->setTooltip ("Program select");
	cbxPrograms->setEditableText (false);
	cbxPrograms->setJustificationType (Justification::centredLeft);
	cbxPrograms->setTextWhenNothingSelected (String::empty);
	cbxPrograms->setTextWhenNoChoicesAvailable ("(no choices)");
	cbxPrograms->setColour (ComboBox::backgroundColourId, Colour (0xff000000));
	cbxPrograms->setColour (ComboBox::textColourId, Colour (0xe0ffffff));
	cbxPrograms->setColour (ComboBox::buttonColourId, Colour (0x80ffffff));
	cbxPrograms->setColour (ComboBox::outlineColourId, Colour (0x80ffffff));
	cbxPrograms->setColour (PopupMenu::backgroundColourId, Colour (0xff000000));
	cbxPrograms->setColour (PopupMenu::highlightedBackgroundColourId, Colour (0xff8ddeff));
	cbxPrograms->setColour (PopupMenu::highlightedTextColourId, Colour (0xfff0f0f0));
	for (int i = 0; i < ownerFilter->getNumPrograms(); i++)
	{
		cbxPrograms->addItem (ownerFilter->getProgramName (i), i+1);
	}
	cbxPrograms->addListener (this);

	addAndMakeVisible (sldVoices = new Slider ("new slider"));
	sldVoices->setTooltip ("Voices");
	sldVoices->setRange (1, maxVoices, 1);
	sldVoices->setLookAndFeel (laf_number);
	sldVoices->setSliderStyle (Slider::RotaryVerticalDrag);
	sldVoices->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldVoices->addListener (this);

	addAndMakeVisible (tglSyncLFO = new ToggleButton ("SYNC"));
	tglSyncLFO->setTooltip ("Sync LFO");
	tglSyncLFO->addListener (this);

	addAndMakeVisible (DTS = new DottedTripletSelector());
	DTS->setSyncEnabled (false);
	DTS->addListener (this);

	addAndMakeVisible (lblGlide = new Label ("new label", "GLIDE"));
	lblGlide->setFont (Font (15.00f, Font::plain));
	lblGlide->setJustificationType (Justification::centredLeft);
	lblGlide->setEditable (false, false, false);

	addAndMakeVisible (sldGlide = new Slider ("new slider"));
	sldGlide->setTooltip ("Glide");
	sldGlide->setRange (0, 1, 0);
	sldGlide->setLookAndFeel (laf);
	sldGlide->setSliderStyle (Slider::RotaryVerticalDrag);
	sldGlide->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
	sldGlide->addListener (this);

	for (int i = 0; i < kOps; i++)
	{
		arOG.add (new OperatorGUI (i, ownerFilter->getWT (0)));
		addAndMakeVisible (arOG.getLast());
		arOG.getLast()->addListener (this);
	}

	setSize (776 + kOps * 32, 116 + kOps * 32);
	updateGUI();
	startTimer (refreshGUI);
}

JelaniFMSynthAudioProcessorEditor::~JelaniFMSynthAudioProcessorEditor()
{
	imgComponentLogo = nullptr;
	lblWForm = nullptr;
	lblRatio = nullptr;
	lblSTOff = nullptr;
	lblHzOff = nullptr;
	lblLFOPi = nullptr;
	lblPEAmt = nullptr;
	lblPhase = nullptr;
	lblAmp = nullptr;
	lblVS = nullptr;
	lblKey = nullptr;
	lblLFO = nullptr;
	lblKTi = nullptr;
	lblAtt = nullptr;
	lblDec = nullptr;
	lblSus = nullptr;
	lblRel = nullptr;
	lblMod = nullptr;
	lblOut = nullptr;
	lblPgm = nullptr;
	lblVcs = nullptr;
	lblPEn = nullptr;
	lblLSp = nullptr;
	lblLastAction = nullptr;
	btnLoad = nullptr;
	btnSave = nullptr;
	btnReset = nullptr;
	sldPEAttck = nullptr;
	sldPEDecay = nullptr;
	sldLFOSpeed = nullptr;
	sldOutLevel = nullptr;
	cbxPrograms = nullptr;
	sldVoices = nullptr;
	tglSyncLFO = nullptr;
	DTS = nullptr;
	lblGlide = nullptr;
	sldGlide = nullptr;

	arOG.clear();
}

void JelaniFMSynthAudioProcessorEditor::paint (Graphics& g)
{
	// background
	g.fillAll (Colours::black);
	
	// top row
	g.setColour (Colour (0xff505050));
	g.fillRoundedRectangle (rctPGM, 3.f);
	g.fillRoundedRectangle (rctVCS, 3.f);
	
	// bottom row
	g.setColour (Colour (0xffb0b0b0));
	//g.fillRect (sldLFOSpeed->getBounds().expanded(3));
	g.fillRoundedRectangle (rctLFO, 3.f);
	g.fillRoundedRectangle (rctPEN, 3.f);
	if (glideEnabled)
		g.fillRoundedRectangle (rctGLIDE, 3.f);
}

void JelaniFMSynthAudioProcessorEditor::resized ()
{
	imgComponentLogo->setBounds (16, 4, 147, 37);
	int x = 8, y = 42;
	tglApplyToAll->setBounds (x, y + 8, 16, 16); x += 20;
	lblWForm->setBounds (x, y, 60, 32); x += 60;
	lblRatio->setBounds (x, y, 60, 32); x += 60;
	lblSTOff->setBounds (x, y, 60, 32); x += 60;
	lblHzOff->setBounds (x, y, 60, 32); x += 60;
	lblLFOPi->setBounds (x, y, 32, 32); x += 32; //int lfox = x;
	lblPEAmt->setBounds (x, y, 32, 32); x += 32;
	lblPhase->setBounds (x, y, 32, 32); x += 32;
	lblAmp->setBounds (x, y, 32, 32); x += 32;
	lblVS->setBounds (x, y, 32, 32); x += 32;
	lblKey->setBounds (x, y, 32, 32); x += 32;
	lblLFO->setBounds (x, y, 32, 32); x += 32;
	lblKTi->setBounds (x, y, 32, 32); x += 36; //int attx = x;
	lblAtt->setBounds (x, y, 32, 32); x += 32;
	lblDec->setBounds (x, y, 32, 32); x += 32;
	lblSus->setBounds (x, y, 32, 32); x += 32;
	lblRel->setBounds (x, y, 32, 32); x += 80;
	lblMod->setBounds (x, y, 60, 32); x += 32 * kOps - 4;
	lblOut->setBounds (x, y, 60, 32); x += 40; y += 30; int ogwidth = x - 4;
	sldOutLevel->setBounds (x, y, 32, kOps * 32);
	for (int i = 0; i < kOps; i++)
		arOG[i]->setBounds(1, y + i * 32, ogwidth, 32);

	int lfox = (int)(ogwidth * .4);
	x = lfox; y += kOps * 32 + 9;
	lblLSp->setBounds (x, y, 72, 24); x += 82;
	sldLFOSpeed->setBounds (x, y, 24, 24); x += 32;
	tglSyncLFO->setBounds (x, y, 48, 24); x += 56;
	DTS->setBounds (x, y, 24, 24);

	int attx = (int)(ogwidth * .64);
	lblPEn->setBounds (attx, y, 72, 24); attx += 80;
	sldPEAttck->setBounds (attx, y, 24, 24); attx += 32;
	sldPEDecay->setBounds (attx, y, 24, 24); attx += 56;
	lblGlide->setBounds (attx, y, 56, 24); attx += 56;
	sldGlide->setBounds (attx, y, 24, 24);

	lblLastAction->setBounds (0, y, 512, 24);

	int pgmx = (int)(ogwidth * .26);
	y = 12; x = pgmx;
	lblPgm->setBounds (x, y - 4, 80, 32); x += 82;
	cbxPrograms->setBounds (x, y, 192, 24); x += 192 + 8;
	btnLoad->setBounds (x, y, 24, 24); x += 30;
	btnSave->setBounds (x, y, 24, 24); x += 30 + 8;
	btnReset->setBounds (x, y, 24, 24); x += 80;

	lblVcs->setBounds (x, y - 4, 64, 32); x += 70;
	sldVoices->setBounds (x, y, 32, 24);

	rctLFOSYNC = sldLFOSpeed->getBounds().getUnion (DTS->getBounds()).expanded (3).toFloat();
	rctLFONOSYNC = sldLFOSpeed->getBounds().getUnion (tglSyncLFO->getBounds()).expanded (3).toFloat();
	rctLFO = rctLFONOSYNC;
	rctPEN = sldPEAttck->getBounds().getUnion (sldPEDecay->getBounds()).expanded (3).toFloat();
	rctGLIDE = sldGlide->getBounds().expanded (3).toFloat();
	rctPGM = cbxPrograms->getBounds().getUnion (btnReset->getBounds()).expanded (4).toFloat();
	rctVCS = sldVoices->getBounds().expanded (4).toFloat();
}

void JelaniFMSynthAudioProcessorEditor::sliderValueChanged (Slider* sliderThatWasMoved)
{
	JelaniFMSynthAudioProcessor* ourProcessor = getProcessor();
	if (sliderThatWasMoved == sldOutLevel)
	{
		ourProcessor->setParameterInternalNotifyingHost(JelaniFMSynthAudioProcessor::OutLevel, (float)sliderThatWasMoved->getValue());
		lblLastAction->setText ("Out level: " + String(sliderThatWasMoved->getValue(), 2) + " (dB)", dontSendNotification);
	}
	else if (sliderThatWasMoved == sldLFOSpeed)
	{
		if (ourProcessor->getParameter (JelaniFMSynthAudioProcessor::LFOSync) > 0.5)
		{
			ourProcessor->setParameterInternalNotifyingHost (JelaniFMSynthAudioProcessor::LFOSyncId, (float)sliderThatWasMoved->getValue());
			lblLastAction->setText (
					"LFO Speed: " +
					String (LFO::getSyncSpeedName ((int)sliderThatWasMoved->getValue(), (int)ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::LFOSyncMultId))) + 
					" " +
					String (ourProcessor->getLFOSpeed(), 4) + " (Hz)",
					dontSendNotification);
		}
		else
		{
			ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::LFOSpeed, (float)sliderThatWasMoved->getValue());
			lblLastAction->setText ("LFO Speed: " + String (ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::LFOSpeed), 4) + " (Hz)", dontSendNotification);
		}
	}
	else if (sliderThatWasMoved == sldPEAttck)
	{
		ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::PEAttck, (float)sliderThatWasMoved->getValue());
		lblLastAction->setText ("Pitch attack: " + String(ourProcessor->getParameterInternal(JelaniFMSynthAudioProcessor::PEAttck), 3) + " (ms)", dontSendNotification);
	}
	else if (sliderThatWasMoved == sldPEDecay)
	{
		ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::PEDecay, (float)sliderThatWasMoved->getValue());
		lblLastAction->setText ("Pitch decay: " + String(ourProcessor->getParameterInternal(JelaniFMSynthAudioProcessor::PEDecay), 3) + " (ms)", dontSendNotification);
	}
	else if (sliderThatWasMoved == sldVoices)
	{
		ourProcessor->setParameterInternalNotifyingHost (JelaniFMSynthAudioProcessor::NumVoices, (float)sliderThatWasMoved->getValue());
		lblLastAction->setText ("Voices: " + String ((int)ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::NumVoices)), dontSendNotification);

		if (glideEnabled != (sliderThatWasMoved->getValue() == 1))
		{
			glideEnabled = sliderThatWasMoved->getValue() == 1;
			sldGlide->setVisible (glideEnabled);
			lblGlide->setVisible (glideEnabled);
			repaint();
		}
	}
	else if (sliderThatWasMoved == sldGlide)
	{
		ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::GlideTime, (float)sliderThatWasMoved->getValue());
		lblLastAction->setText ("Glide: " + String (ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::GlideTime), 2) + " (ms)", dontSendNotification);
	}
}

void JelaniFMSynthAudioProcessorEditor::buttonClicked (Button* buttonThatWasClicked)
{
	JelaniFMSynthAudioProcessor* ourProcessor = getProcessor ();
	if (buttonThatWasClicked == btnLoad)
	{
		FileChooser loadFC("Load preset", ourProcessor->getSettingsDirectory(), "*.xml");
		if (loadFC.browseForFileToOpen())
		{
			File loadedFile = loadFC.getResult ();
			XmlElement* xml = XmlDocument::parse (loadedFile);
			if (xml != nullptr)
				ourProcessor->setState (xml);
			lblLastAction->setText ("Loaded " + loadedFile.getFileName(), juce::dontSendNotification);
		}
	}
	else if (buttonThatWasClicked == btnSave)
	{
		FileChooser saveFC ("Save preset", ourProcessor->getSettingsDirectory(), "*.xml");
		if (saveFC.browseForFileToSave(true))
		{
			File savedFile = saveFC.getResult();
			ourProcessor->getState()->writeToFile (savedFile, "");
			lblLastAction->setText ("Saved " + savedFile.getFileName(), juce::dontSendNotification);
		}
	}
	else if (buttonThatWasClicked == btnReset)
	{
		ourProcessor->paramReset ();
	}
}

void JelaniFMSynthAudioProcessorEditor::buttonStateChanged (Button* button)
{
	if (button == tglSyncLFO)
	{
		JelaniFMSynthAudioProcessor* ourProcessor = getProcessor ();
		if (ourProcessor->getParameter (JelaniFMSynthAudioProcessor::LFOSync) != (button->getToggleState() ? 1. : 0.))
		{
			bool en = button->getToggleState();
			DTS->setSyncEnabled (en);
			if (en)
			{
				ourProcessor->setParameter (JelaniFMSynthAudioProcessor::LFOSync, 1.f);

				lblLastAction->setText (
						"LFO Speed: " +
						LFO::getSyncSpeedName ((int)ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::LFOSyncId), (int)ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::LFOSyncMultId)) + 
						" " +
						String (ourProcessor->getLFOSpeed(), 4) + " (Hz)",
						dontSendNotification);
				updateSyncState();
			}
			else
			{
				ourProcessor->setParameter (JelaniFMSynthAudioProcessor::LFOSync, 0.f);

				lblLastAction->setText ("LFO Speed: " + String (ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::LFOSpeed), 4) + " (Hz)", dontSendNotification);
				updateSyncState();
			}
		}
	}
}

void JelaniFMSynthAudioProcessorEditor::comboBoxChanged (ComboBox* cbx)
{
	if (cbx == cbxPrograms)
	{
		JelaniFMSynthAudioProcessor* ourProcessor = getProcessor();
		int newIndex = cbx->getSelectedItemIndex();
		if (newIndex >= 0)
		{
			ourProcessor->setCurrentProgram (newIndex);
			lblLastAction->setText ("Loaded " + cbx->getText(), dontSendNotification);
		}
	}
}

void JelaniFMSynthAudioProcessorEditor::timerCallback()
{
	JelaniFMSynthAudioProcessor* ourProcessor = getProcessor();
	if (ourProcessor->NeedsUIUpdate())
	{
		updateGUI();
		ourProcessor->ClearUIUpdateFlag();
	}
}

void JelaniFMSynthAudioProcessorEditor::updateGUI()
{
	JelaniFMSynthAudioProcessor* ourProcessor = getProcessor();
	sldOutLevel->setValue (ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::OutLevel), juce::dontSendNotification);
	sldPEAttck->setValue (ourProcessor->getParameter (JelaniFMSynthAudioProcessor::PEAttck), dontSendNotification);
	sldPEDecay->setValue (ourProcessor->getParameter (JelaniFMSynthAudioProcessor::PEDecay), dontSendNotification);
	sldVoices->setValue (ourProcessor->getNumVoices());

	bool tmp = ourProcessor->getGlideEnabled();
	if (glideEnabled != tmp)
	{
		glideEnabled = tmp;
		sldGlide->setVisible (glideEnabled);
		lblGlide->setVisible (glideEnabled);
		repaint();
	}

	if (tglSyncLFO->getToggleState() != (ourProcessor->getParameter (JelaniFMSynthAudioProcessor::LFOSync) > .5f))
	{
		tglSyncLFO->setToggleState (ourProcessor->getParameter (JelaniFMSynthAudioProcessor::LFOSync) > .5f, juce::dontSendNotification);
		DTS->setSyncEnabled (tglSyncLFO->getToggleState());
		updateSyncState();
	}
	else
		updateLFOSpeedValue();

	for (int op = 0; op < kOps; op++)
	{
		for (int pr = 0; pr < Operator::totalNumParamsPerOperator; pr++)
		{
			if (pr == Operator::Ratio || pr == Operator::STOff || pr == Operator::HzOff)
			{
				double v = ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::startOfOpParams + op * Operator::totalNumParamsPerOperator + pr);
				arOG[op]->setOpValue (pr, v, juce::dontSendNotification);
			}
			else if (pr == Operator::WForm)
			{
				arOG[op]->setOpValue (pr, ourProcessor->getParameter (JelaniFMSynthAudioProcessor::startOfOpParams + op * Operator::totalNumParamsPerOperator + pr), juce::dontSendNotification);
				arOG[op]->setWT ((int)ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::startOfOpParams + op * Operator::totalNumParamsPerOperator + pr));
			}
			else
				arOG[op]->setOpValue (pr, ourProcessor->getParameter (JelaniFMSynthAudioProcessor::startOfOpParams + op * Operator::totalNumParamsPerOperator + pr), juce::dontSendNotification);
		}
		for (int dt = 0; dt < kOps; dt++)
		{
			arOG[op]->setModValue (dt, ourProcessor->getParameter (JelaniFMSynthAudioProcessor::startOfModParams + op * kOps + dt), juce::dontSendNotification);
		}
	}
}

void JelaniFMSynthAudioProcessorEditor::updateSyncState()
{
	JelaniFMSynthAudioProcessor* ourProcessor = getProcessor();
	bool syncOn = (ourProcessor->getParameter (JelaniFMSynthAudioProcessor::LFOSync) > .5f);
	if (syncOn)
	{
		DTS->setSMID ((LFO::LFOSyncMultIds)(int)ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::LFOSyncMultId));
		sldLFOSpeed->setRange (LFO::Four, LFO::One32nd, 1.f);
		sldLFOSpeed->setSkewFactor (1);
		sldLFOSpeed->setValue (ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::LFOSyncId), juce::dontSendNotification);
		rctLFO = rctLFOSYNC; repaint();
	}
	else
	{
		sldLFOSpeed->setRange (0, 1, 0);
		sldLFOSpeed->setSkewFactor (defaultLFOSpeedSkew);
		sldLFOSpeed->setValue (ourProcessor->getParameter (JelaniFMSynthAudioProcessor::LFOSpeed), juce::dontSendNotification);
		rctLFO = rctLFONOSYNC; repaint();
	}
}

void JelaniFMSynthAudioProcessorEditor::updateLFOSpeedValue ()
{
	JelaniFMSynthAudioProcessor* ourProcessor = getProcessor ();
	bool syncOn = (ourProcessor->getParameter (JelaniFMSynthAudioProcessor::LFOSync) > .5f);
	if (syncOn)
		sldLFOSpeed->setValue (ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::LFOSyncId), juce::dontSendNotification);
	else
		sldLFOSpeed->setValue (ourProcessor->getParameter (JelaniFMSynthAudioProcessor::LFOSpeed), juce::dontSendNotification);
}

void JelaniFMSynthAudioProcessorEditor::operatorParameterChanged (int oIdx, int pIdx, float newValue)
{
	JelaniFMSynthAudioProcessor* ourProcessor = getProcessor();
	bool ATA = tglApplyToAll->getToggleState();
	if (ATA)
	{
		if (pIdx == Operator::Ratio || pIdx == Operator::STOff || pIdx == Operator::HzOff)
		{
			for (int i = 0; i < kOps; i++)
			{
				ourProcessor->setParameterInternalNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams +
					i * Operator::totalNumParamsPerOperator +
					pIdx,
					newValue);

				if (i != oIdx)
					arOG[i]->setOpValue (pIdx, newValue, dontSendNotification);
			}

			lblLastAction->setText (String ("All ") + Operator::getFriendlyParameterName (pIdx) + ": " + String (newValue, 2) + " " + Operator::getFriendlyParameterUnit (pIdx), juce::dontSendNotification);
		}
		else if (pIdx == Operator::Activ) // activate operator: not 'applied to all'
		{
			ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams +
				oIdx * Operator::totalNumParamsPerOperator +
				pIdx,
				newValue);
			lblLastAction->setText ("Operator " + String (Operator::getOperatorName (oIdx)) + " " + (newValue > 0.5 ? "On" : "Off"), dontSendNotification);
		}
		else
		{
			for (int i = 0; i < kOps; i++)
			{
				ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams +
					i * Operator::totalNumParamsPerOperator +
					pIdx,
					newValue);

				if (i != oIdx)
					arOG[i]->setOpValue (pIdx, newValue, dontSendNotification);
			}

			float processedValue = ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::startOfOpParams + oIdx * Operator::totalNumParamsPerOperator + pIdx);

			if (pIdx == Operator::Phase || pIdx == Operator::VSens)
				lblLastAction->setText (String ("All ") + Operator::getFriendlyParameterName (pIdx) + ": " + String (100 * processedValue, 2) + "%", juce::dontSendNotification);
			else if (pIdx == Operator::Level || pIdx == Operator::Sustn || pIdx == Operator::PostA)
			{
				String dBStr;
				switch (pIdx)
				{
				case Operator::Level:	dBStr = (processedValue <= opLevelMinInfdB) ? "-INF" : String (processedValue, 2);
				case Operator::Sustn:	dBStr = (processedValue <= opSustnMinInfdB) ? "-INF" : String (processedValue, 2);
				case Operator::PostA:	dBStr = (processedValue <= opPostAMinInfdB) ? "-INF" : String (processedValue, 2);
				}
				lblLastAction->setText (String ("All ") + Operator::getFriendlyParameterName (pIdx) + ": " + dBStr + " " + Operator::getFriendlyParameterUnit (pIdx), juce::dontSendNotification);
			}
			else if (pIdx == Operator::WForm)
			{
				lblLastAction->setText (String ("All ") + Operator::getFriendlyParameterName (pIdx) + ": " + Operator::getFriendlyWFName ((int)processedValue), juce::dontSendNotification);
				arOG[oIdx]->setWT ((int)processedValue);
			}
			else
				lblLastAction->setText (String ("All ") + Operator::getFriendlyParameterName (pIdx) + ": " + String (processedValue, 2) + " " + Operator::getFriendlyParameterUnit (pIdx), juce::dontSendNotification);
		}
	}
	else
	{
		if (pIdx == Operator::Ratio || pIdx == Operator::STOff || pIdx == Operator::HzOff)
		{
			ourProcessor->setParameterInternalNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams +
				oIdx * Operator::totalNumParamsPerOperator +
				pIdx,
				newValue);

			lblLastAction->setText ("Operator " + String (Operator::getOperatorName (oIdx)) + " " + Operator::getFriendlyParameterName (pIdx) + ": " + String (newValue, 2) + " " + Operator::getFriendlyParameterUnit (pIdx), juce::dontSendNotification);
		}
		else if (pIdx == Operator::Activ)
		{
			ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams +
				oIdx * Operator::totalNumParamsPerOperator +
				pIdx,
				newValue);
			lblLastAction->setText ("Operator " + String (Operator::getOperatorName (oIdx)) + " " + (newValue > 0.5 ? "On" : "Off"), dontSendNotification);
		}
		else
		{
			ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams +
				oIdx * Operator::totalNumParamsPerOperator +
				pIdx,
				newValue);

			float processedValue = ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::startOfOpParams + oIdx * Operator::totalNumParamsPerOperator + pIdx);

			if (pIdx == Operator::Phase || pIdx == Operator::VSens)
				lblLastAction->setText ("Operator " + String (Operator::getOperatorName (oIdx)) + " " + Operator::getFriendlyParameterName (pIdx) + ": " + String (100 * processedValue, 2) + "%", juce::dontSendNotification);
			else if (pIdx == Operator::Level || pIdx == Operator::Sustn || pIdx == Operator::PostA)
			{
				String dBStr;
				switch (pIdx)
				{
				case Operator::Level:	dBStr = (processedValue <= opLevelMinInfdB) ? "-INF" : String (processedValue, 2);
				case Operator::Sustn:	dBStr = (processedValue <= opSustnMinInfdB) ? "-INF" : String (processedValue, 2);
				case Operator::PostA:	dBStr = (processedValue <= opPostAMinInfdB) ? "-INF" : String (processedValue, 2);
				}
				lblLastAction->setText ("Operator " + String (Operator::getOperatorName (oIdx)) + " " + Operator::getFriendlyParameterName (pIdx) + ": " + dBStr + " " + Operator::getFriendlyParameterUnit (pIdx), juce::dontSendNotification);
			}
			else if (pIdx == Operator::WForm)
			{
				lblLastAction->setText ("Operator " + String (Operator::getOperatorName (oIdx)) + " " + Operator::getFriendlyParameterName (pIdx) + ": " + Operator::getFriendlyWFName ((int)processedValue), juce::dontSendNotification);
				arOG[oIdx]->setWT ((int)processedValue);
			}
			else
				lblLastAction->setText ("Operator " + String (Operator::getOperatorName (oIdx)) + " " + Operator::getFriendlyParameterName (pIdx) + ": " + String (processedValue, 2) + " " + Operator::getFriendlyParameterUnit (pIdx), juce::dontSendNotification);
		}
	}
}

void JelaniFMSynthAudioProcessorEditor::modParameterChanged(int fromIdx, int toIdx, float newValue)
{
	JelaniFMSynthAudioProcessor* ourProcessor = getProcessor();
	ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfModParams +
		fromIdx * kOps +
		toIdx,
		newValue);

	float processedValue = ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::startOfModParams + fromIdx * kOps + toIdx);

	String dBStr = (processedValue <= opModulMinInfdB) ? "-INF" : String (processedValue, 2);
	lblLastAction->setText ("Operator " + String (Operator::getOperatorName (fromIdx)) + ">" + String (Operator::getOperatorName (toIdx)) + ": " + dBStr + " (dB)", juce::dontSendNotification);
}

void JelaniFMSynthAudioProcessorEditor::copyEnvelopeBuffer (int fromIdx, float *ADSR)
{
	for (int i = 1; i < ADSR::totalNumEnvPhases; i++) // state 0 is 'idle' - it doesn't have a value
	{
		envBuf[i] = ADSR[i];
	}
	lblLastAction->setText ("Envelope copied from " + String (Operator::getOperatorName (fromIdx)), dontSendNotification);
}

void JelaniFMSynthAudioProcessorEditor::pasteEnvelopeBuffer (int toIdx)
{
	JelaniFMSynthAudioProcessor* ourProcessor = getProcessor();

	ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams +
		toIdx * Operator::totalNumParamsPerOperator +
		Operator::Attck,
		envBuf[ADSR::Attack]);
	ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams +
		toIdx * Operator::totalNumParamsPerOperator +
		Operator::Decay,
		envBuf[ADSR::Decay]);
	ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams +
		toIdx * Operator::totalNumParamsPerOperator +
		Operator::Sustn,
		envBuf[ADSR::Sustain]);
	ourProcessor->setParameterNotifyingHost (JelaniFMSynthAudioProcessor::startOfOpParams +
		toIdx * Operator::totalNumParamsPerOperator +
		Operator::Relea,
		envBuf[ADSR::Release]);

	arOG[toIdx]->setADSR (envBuf);
	lblLastAction->setText ("Envelope pasted to " + String (Operator::getOperatorName (toIdx)), dontSendNotification);
}

void JelaniFMSynthAudioProcessorEditor::syncMultIdChanged (LFO::LFOSyncMultIds newSyncMultId)
{
	JelaniFMSynthAudioProcessor* ourProcessor = getProcessor();

	ourProcessor->setParameterInternal (JelaniFMSynthAudioProcessor::LFOSyncMultId, (float)newSyncMultId);
	if (tglSyncLFO->getToggleState())
		lblLastAction->setText (
						"LFO Speed: " +
						String (LFO::getSyncSpeedName ((int)ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::LFOSyncMultId), (int)ourProcessor->getParameterInternal (JelaniFMSynthAudioProcessor::LFOSyncMultId))) + 
						" " +
						String (ourProcessor->getLFOSpeed(), 4) + " (Hz)",
						dontSendNotification);
}
