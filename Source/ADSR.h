/*
  ==============================================================================

	ADSR.h
	Created: 30 Jan 2014 9:05:51pm
	Author:  Cas Cassette

	This file is part of Jelani FM Synthesizer.
	Copyright (c) 2014 - Sine Dubio Creative Studios

  ==============================================================================
*/

#ifndef ADSR_H_INCLUDED
#define ADSR_H_INCLUDED

class ADSR
{
public:
	ADSR ();

	// Must be called before setStuff, process/getOutput functions
	void setSampleRate (double sampleRate);

	// Reacts to midi input from FMVoice (via corresponding Operator)
	void noteOn (float f);    // f is key-scaled time factor
	void noteOff();

	// Advances sample and returns new gain value
	float process();
	// Returns gain value without advancing through time
	float getOutput() { return output; }

	// Getters & setters
	float getAttack()  { return att; }
	float getDecay()   { return dec; }
	float getSustain() { return sus; }
	float getRelease() { return rel; }
	int   getState()   { return state; }

	void setAttack (float newAtt);
	void setDecay	(float newDec);
	void setSustain (float newSus);
	void setRelease (float newRel);

	// Kill env state
	void reset ();

	enum Phases
	{
		Idle = 0,
		Attack,
		Decay,
		Sustain,
		Release,

		totalNumEnvPhases
	};

protected:
	int state;
	bool isNoteOn;
	float output;
	float att, dec, sus, rel;
	double deltaGAtt, deltaGDec, deltaGRel;
	//float attackCoef, decayCoef, releaseCoef;
	//float targetRatioA, targetRatioDR;
	//float attackBase, decayBase, releaseBase;
	float factor; // from keyscaling

	//float calcCoef (float rate, float targetRatio);

	// samples / msec
	double spms;
};

#endif	// ADSR_H_INCLUDED
